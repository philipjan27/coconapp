package coconut.app.coconapp.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.Date;

import coconut.app.coconapp.Classes.PricePerGallon;
import coconut.app.coconapp.Classes.ProductsType;
import coconut.app.coconapp.Database.DatabaseHelper;
import coconut.app.coconapp.R;

public class EditLiquidProductsActivity extends AppCompatActivity  {


    DatabaseHelper helper;
    Dao<PricePerGallon,Integer> pricePerGallonIntegerDao;
    PricePerGallon pricePerGallonObject, pricePerGallonObj;

    Button saveEditedValue, cancelEditedValue;
    EditText numberOftrees, numberOfCoconuts, pricePerLiter, numberOfLiters, labor;
    Spinner cocoVariety, cocoSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_liquid_products);

        Toolbar toolbar=(Toolbar) findViewById(R.id.editliquidProducts_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Edit Product");

         saveEditedValue= (Button)findViewById(R.id.buttonsave_liquidProducts);
         cancelEditedValue= (Button)findViewById(R.id.buttoncancel_liquidProducts);
         numberOftrees= (EditText)findViewById(R.id.pricerpergallon_numberoftrees_editext);
         numberOfCoconuts= (EditText)findViewById(R.id.pricerpergallon_numberofcoconut_editext);
        cocoVariety= (Spinner)findViewById(R.id.pricepergallon_coconutvariety_spinner);
        cocoSize= (Spinner)findViewById(R.id.pricepergallon_coconutsize_spinner);
         numberOfLiters= (EditText)findViewById(R.id.pricepergallon_numberofliters_editext);
         pricePerLiter= (EditText)findViewById(R.id.pricepergallon_priceperliter_editext);
         labor= (EditText)findViewById(R.id.pricepergallon_labor_editext);

       if (pricePerGallonObj == null) {
            pricePerGallonObj = (PricePerGallon) getIntent().getSerializableExtra("object");
        }else {
            pricePerGallonObj=null;
        }

        //Configuring Coconut Variety
        ArrayAdapter cocoVarietyAdapter= ArrayAdapter.createFromResource(getBaseContext(),R.array.coconut_varieties,android.R.layout.simple_spinner_item);
        cocoVarietyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cocoVariety.setAdapter(cocoVarietyAdapter);

        //Configuring Coconut Sizes adapter
        ArrayAdapter cocoSizeAdapter= ArrayAdapter.createFromResource(getBaseContext(),R.array.coconut_sizes,android.R.layout.simple_spinner_item);
        cocoSizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cocoSize.setAdapter(cocoSizeAdapter);

        setValues();

                saveEditedValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveValueChanges();
                clearUIWidgetContents();

            }
        });

        cancelEditedValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            cancelDataEdit();
            }
        });
    }

    private DatabaseHelper getHelper() {
        if (helper == null) {
            helper= OpenHelperManager.getHelper(this,DatabaseHelper.class);
        }

        return helper;
    }

    private void saveValueChanges() {
        // Getting
        ProductsType productsType= new ProductsType(LiquidProductsActivity.PRICE_PER_GALLON_ID);


        int idval,numOfTreesVal, numOfCocoVal;
        double numOfLitersVal,priceperLiterVal, laborVal, initialIncome;
        String cocoVarietyVal, cocoSizeVal;
        Date dateCreatedval;

        idval= pricePerGallonObj.getId();
        numOfTreesVal= Integer.parseInt(numberOftrees.getText().toString());
        numOfCocoVal= Integer.parseInt(numberOfCoconuts.getText().toString());
        dateCreatedval= LiquidProductsActivity.getCurrentTimeStamp();
        cocoVarietyVal= cocoVariety.getSelectedItem().toString();
        cocoSizeVal= cocoSize.getSelectedItem().toString();
        numOfLitersVal= Double.parseDouble(numberOfLiters.getText().toString());
        priceperLiterVal= Double.parseDouble(pricePerLiter.getText().toString());
        laborVal= Double.parseDouble(labor.getText().toString());

        initialIncome= (numOfLitersVal*priceperLiterVal)-laborVal;

        pricePerGallonObject= new PricePerGallon(idval,productsType,numOfTreesVal,numOfCocoVal,cocoVarietyVal,cocoSizeVal, numOfLitersVal, priceperLiterVal, laborVal,initialIncome,dateCreatedval);

        try {
            pricePerGallonIntegerDao= getHelper().getPricePerGallonIntegerDao();
            int result=pricePerGallonIntegerDao.update(pricePerGallonObject);
            pricePerGallonIntegerDao.refresh(pricePerGallonObject);

            Log.i("Update Result",String.valueOf(result));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        startActivity(new Intent(EditLiquidProductsActivity.this,LiquidProductsActivity.class));


    }

    private void setValues() {

        numberOftrees.setText(String.valueOf(pricePerGallonObj.getPricePerGallon_numberOfTree()));
        numberOfCoconuts.setText(String.valueOf(pricePerGallonObj.getPricePerGallon_numberOfCoconut()));
        numberOfLiters.setText(String.valueOf(pricePerGallonObj.getPricePerGallon_numberOfLiters()));
        pricePerLiter.setText(String.valueOf(pricePerGallonObj.getPricePerGallon_pricePerLiter()));
        labor.setText(String.valueOf(pricePerGallonObj.getPricePerGallon_laborPrice()));

    }

    private void cancelDataEdit() {
        startActivity(new Intent(EditLiquidProductsActivity.this,LiquidProductsActivity.class));
    }

    private void clearUIWidgetContents() {
        numberOftrees.setText("");
        numberOfCoconuts.setText("");
        pricePerLiter.setText("");
        numberOfLiters.setText("");
        labor.setText("");
    }

}
