package coconut.app.coconapp.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.Date;

import coconut.app.coconapp.Classes.PricePerFruit;
import coconut.app.coconapp.Classes.ProductsType;
import coconut.app.coconapp.Database.DatabaseHelper;
import coconut.app.coconapp.R;

public class EditPricePerFruitActivity extends AppCompatActivity {


    EditText numberOfTrees, numberOfCoconuts, pricePerFruit,labor;
    Spinner cocoVariety,cocoSize;
    Button saveButton, cancelButton;
    DatabaseHelper dbHelper=null;
    Dao<PricePerFruit, Integer> pricePerFruitIntegerDao;
    Toolbar tb;
    PricePerFruit pricePerFruitObj= null;
    PricePerFruit pricePerFruitUpdatedValues=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_price_per_fruit);

        tb= (Toolbar)findViewById(R.id.editproductper_fruit_toolbar);
        setSupportActionBar(tb);

        saveButton= (Button)findViewById(R.id.button_priceperfruit_save);
        cancelButton= (Button)findViewById(R.id.button_priceperfruit_cancel);

        numberOfTrees = (EditText)findViewById(R.id.priceperfruit_numberof_tree);
        numberOfCoconuts= (EditText)findViewById(R.id.priceperfruit_numberofcoconut);
        pricePerFruit= (EditText)findViewById(R.id.priceperfruit_priceperfruit_editext);
        labor= (EditText)findViewById(R.id.priceperfruit_labor_editext);

        cocoVariety= (Spinner)findViewById(R.id.priceperfruit_fruitvariety_spinner);
        cocoSize= (Spinner)findViewById(R.id.priceperfruit_fruitsize_spinner);

        setupCocoSizeSpinner();
        setupCocovarietySpinner();
        setWidgetValues();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveValues();
                startActivity(new Intent(EditPricePerFruitActivity.this,SolidProductsActivity.class));
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EditPricePerFruitActivity.this, SolidProductsActivity.class));
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (dbHelper != null) {
            OpenHelperManager.releaseHelper();
            dbHelper=null;
        }
        super.onDestroy();
    }

    /** Getting Database Helper*/
    public DatabaseHelper getHelper() {
        if (dbHelper == null) {
            dbHelper= OpenHelperManager.getHelper(this,DatabaseHelper.class);
        }

        return dbHelper;
    }

    /** Setup CocoVariety */
    public void setupCocovarietySpinner() {
        ArrayAdapter cocoVarietyAdapter= ArrayAdapter.createFromResource(getBaseContext(),R.array.coconut_varieties,android.R.layout.simple_spinner_item);
        cocoVarietyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cocoVariety.setAdapter(cocoVarietyAdapter);
    }
    /** Setup Coco Size Spinner */
    public void setupCocoSizeSpinner() {
        ArrayAdapter cocoSizeAdapter= ArrayAdapter.createFromResource(getBaseContext(),R.array.coconut_sizes,android.R.layout.simple_spinner_item);
        cocoSizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cocoSize.setAdapter(cocoSizeAdapter);
    }


    /** Setting Widget Values */
    private void setWidgetValues() {
        pricePerFruitObj= (PricePerFruit)getIntent().getSerializableExtra("priceperfruitbundle");

        numberOfTrees.setText(String.valueOf(pricePerFruitObj.getPricePerFruit_numberOfTrees()));
        numberOfCoconuts.setText(String.valueOf(pricePerFruitObj.getPricePerFruit_numberOfFruit()));
        pricePerFruit.setText(String.valueOf(pricePerFruitObj.getPricePerFruit_pricePerFruit()));
        labor.setText(String.valueOf(pricePerFruitObj.getLabor()));
    }

    /** saving edited values */
    private void saveValues() {

        int idval,numberOfTreesVal, numberOfFruitsVal;
        double pricePerFruitVal, laborVal,initialIncome;
        String cocoVarietyVal, cocoSizeVal;
        Date dateEditedVal;

        ProductsType productsType= new ProductsType(pricePerFruitObj.getId());

        idval= pricePerFruitObj.getId();
        numberOfTreesVal= Integer.parseInt(numberOfTrees.getText().toString());
        numberOfFruitsVal= Integer.parseInt(numberOfCoconuts.getText().toString());
        pricePerFruitVal= Double.parseDouble(pricePerFruit.getText().toString());
        laborVal= Double.parseDouble(labor.getText().toString());

        cocoVarietyVal= cocoVariety.getSelectedItem().toString();
        cocoSizeVal= cocoSize.getSelectedItem().toString();
        dateEditedVal= LiquidProductsActivity.getCurrentTimeStamp();

        initialIncome= (numberOfFruitsVal*pricePerFruitVal)-laborVal;
        pricePerFruitUpdatedValues= new PricePerFruit(idval, productsType, numberOfTreesVal,numberOfFruitsVal,cocoVarietyVal,
                cocoSizeVal,pricePerFruitVal,laborVal,initialIncome,dateEditedVal);

        try {
            pricePerFruitIntegerDao=getHelper().getPricePerFruitDao();
            pricePerFruitIntegerDao.update(pricePerFruitUpdatedValues);
            pricePerFruitIntegerDao.refresh(pricePerFruitUpdatedValues);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
