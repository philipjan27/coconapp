package coconut.app.coconapp.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.types.IntegerObjectType;

import java.sql.SQLException;
import java.util.Date;

import coconut.app.coconapp.Classes.PricePerKilo;
import coconut.app.coconapp.Classes.ProductsType;
import coconut.app.coconapp.Database.DatabaseHelper;
import coconut.app.coconapp.Fragments.Priceperkilo;
import coconut.app.coconapp.R;

public class EditPricePerKiloValues extends AppCompatActivity {

    DatabaseHelper dbHelper;
    Dao<PricePerKilo, Integer> pricePerKiloIntegerDao;
    PricePerKilo pricePerKilo, pricePerKiloinstance;

    EditText numberOfTrees,numberOfFruits,numberOfSacks, initialKilo, currentpricePerKilo, labor;
    Spinner cocoVariety, cocoSize;
    Button saveButton,cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_price_per_kilo_values);

        Toolbar toolbar= (Toolbar)findViewById(R.id.productsPerKilo_toolbar);
        setSupportActionBar(toolbar);

        saveButton= (Button)findViewById(R.id.button_priceperKilo_save);
        cancelButton= (Button)findViewById(R.id.button_priceperKilo_cancel);

        numberOfTrees= (EditText)findViewById(R.id.priceperkilo_editext_numberoftrees);
        numberOfFruits= (EditText)findViewById(R.id.priceperkilo_editext_numberoffruits);
        numberOfSacks= (EditText)findViewById(R.id.priceperkilo_editext_numberofsacks);
        initialKilo= (EditText)findViewById(R.id.priceperkilo_editext_initialproductkilo);
        currentpricePerKilo= (EditText)findViewById(R.id.priceperkilo_editext_initialpriceperkilo);
        labor= (EditText)findViewById(R.id.priceperkilo_labor_editext);
        cocoVariety= (Spinner)findViewById(R.id.priceperkilo_cocovariety_spinner);
        cocoSize= (Spinner)findViewById(R.id.priceperkilo_cocosize_spinner);

        setupCocoVarietySpinner();
        setupCocoSizeSpinner();
        setWidgetValues();

        /** save button listener */
                saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateValues();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EditPricePerKiloValues.this, SolidProductsActivity.class));
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbHelper != null) {
            OpenHelperManager.releaseHelper();
            dbHelper=null;
        }
    }

    /** Getting DBHelper's Instance */
    public DatabaseHelper getHelper(){
        if (dbHelper == null) {
            dbHelper= OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }

        return dbHelper;
    }

    /** Updating Values */
    private void updateValues() {

        // Getting product id
        ProductsType productsType= new ProductsType(pricePerKilo.getId());

        int idVal, numOfTreesval, numOfFruitsval,numOfSacksVal;
        double initialProdKiloVal, initialPricePerKiloVal, laborVal, initialIncome;
         String cocoVarietyValue,cocoSizeValue;
        Date dateUpdated;

        idVal= pricePerKilo.getId();
        numOfTreesval= Integer.parseInt(numberOfTrees.getText().toString());
        numOfFruitsval= Integer.parseInt(numberOfFruits.getText().toString());
        numOfSacksVal= Integer.parseInt(numberOfFruits.getText().toString());
        initialProdKiloVal =Double.parseDouble(initialKilo.getText().toString());
        initialPricePerKiloVal= Double.parseDouble(currentpricePerKilo.getText().toString());
        laborVal= Double.parseDouble(labor.getText().toString());

        dateUpdated= SolidProductsActivity.getCurrentTimeStamp();
        cocoVarietyValue= cocoVariety.getSelectedItem().toString();
        cocoSizeValue= cocoSize.getSelectedItem().toString();

        initialIncome= (initialProdKiloVal*initialPricePerKiloVal)-laborVal;

        // Instance to be used in saving new values of item
        pricePerKiloinstance= new PricePerKilo(idVal,productsType,numOfTreesval,numOfFruitsval,cocoVarietyValue,cocoSizeValue,numOfSacksVal,initialProdKiloVal,initialPricePerKiloVal,laborVal, initialIncome,dateUpdated);

        try {
            pricePerKiloIntegerDao= getHelper().getPricePerKiloIntegerDao();
           int result= pricePerKiloIntegerDao.update(pricePerKiloinstance); // could not update data 2/11/2017
            pricePerKiloIntegerDao.refresh(pricePerKiloinstance);

            Log.i("Update Result",String.valueOf(result));
        } catch (SQLException e) {
            Log.e("Exception",e.getMessage().toString());
            e.printStackTrace();
        }

        startActivity(new Intent(EditPricePerKiloValues.this, SolidProductsActivity.class));
    }

    /** Setting UI widgets values based on their picked item to be edited
     * Getting the values choosed to be edited or delete*/
    private void setWidgetValues() {

        // Getting the Object to be recieved in this activity send by PriceperkilFragment
         pricePerKilo= (PricePerKilo)getIntent().getSerializableExtra("ppkobject");

        numberOfTrees.setText(String.valueOf(pricePerKilo.getPricePerKilo_numberofTrees()));
        numberOfFruits.setText(String.valueOf(pricePerKilo.getPricePerKilo_numberofCoconutfruit()));
        numberOfSacks.setText(String.valueOf(pricePerKilo.getPricePerKilo_numberOfSacks()));
        initialKilo.setText(String.valueOf(pricePerKilo.getPricePerKilo_initialKilo()));
        currentpricePerKilo.setText(String.valueOf(pricePerKilo.getPricePerKilo_pricePerKilo()));
        labor.setText(String.valueOf(pricePerKilo.getPricePerKilo_laborPrice()));

    }

    /** Setup of Spinner*/
    private void setupCocoVarietySpinner() {
        ArrayAdapter cocoVarietyAdapter= ArrayAdapter.createFromResource(getBaseContext(),R.array.coconut_varieties,android.R.layout.simple_spinner_item);
        cocoVarietyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cocoVariety.setAdapter(cocoVarietyAdapter);
    }

    /** Configuring Coconut Sizes adapter */
    private void setupCocoSizeSpinner() {
        ArrayAdapter cocoSizeAdapter= ArrayAdapter.createFromResource(getBaseContext(),R.array.coconut_sizes,android.R.layout.simple_spinner_item);
        cocoSizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cocoSize.setAdapter(cocoSizeAdapter);
    }

}
