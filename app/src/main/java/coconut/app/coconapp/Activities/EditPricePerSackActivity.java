package coconut.app.coconapp.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.types.IntegerObjectType;

import java.sql.SQLException;
import java.util.Date;

import coconut.app.coconapp.Classes.PricePerFruit;
import coconut.app.coconapp.Classes.PricePerSack;
import coconut.app.coconapp.Classes.ProductsType;
import coconut.app.coconapp.Database.DatabaseHelper;
import coconut.app.coconapp.R;

public class EditPricePerSackActivity extends AppCompatActivity {

    DatabaseHelper dbHelper;
    Dao<PricePerSack, Integer> pricePerSackIntegerDao;
    PricePerSack pricePerSackInstance, priceperSackObj;

    Toolbar pricePersackToolbar;
    Button saveButton,cancelButton;

    EditText numberOfTrees, numberOfFruits, numberOfSack, pricePersack;
    Spinner cocoVariety, cocoSize, cocoProductsType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_price_per_sack);

        pricePersackToolbar= (Toolbar)findViewById(R.id.editproductper_sack_toolbar);
        setSupportActionBar(pricePersackToolbar);

        saveButton= (Button)findViewById(R.id.pricepersack_button_save);
        cancelButton= (Button)findViewById(R.id.pricepersack_button_cancel);

        numberOfTrees= (EditText)findViewById(R.id.pricepersack_numberoftrees);
        numberOfFruits= (EditText)findViewById(R.id.pricepersack_numberoffruits);
        numberOfSack= (EditText)findViewById(R.id.pricepersack_numberofsac_editext);
        pricePersack= (EditText)findViewById(R.id.pricepersack_pricepersack_editext);

        cocoVariety= (Spinner)findViewById(R.id.pricepersack_coconutVariety_spinner);
        cocoSize= (Spinner)findViewById(R.id.pricepersack_fruitsize_spinner);
        cocoProductsType= (Spinner)findViewById(R.id.pricepersack_productType_spinner);

        setupCocoVarietySpinner();
        setupCocoSizeSpinner();
        setupCocoProductsType();
        setWidgetValues();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveUpdatedValues();
                startActivity(new Intent(EditPricePerSackActivity.this, SolidProductsActivity.class));
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EditPricePerSackActivity.this, SolidProductsActivity.class));
            }
        });

        try {
            pricePerSackIntegerDao= getHelper().getPricePerSackIntegerDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (dbHelper != null) {
            OpenHelperManager.releaseHelper();
            dbHelper=null;
        }
    }

    // Getting database helper
    public DatabaseHelper getHelper() {
        if (dbHelper == null) {
            dbHelper= OpenHelperManager.getHelper(this,DatabaseHelper.class);
        }

        return dbHelper;
    }

    public void setWidgetValues() {

        pricePerSackInstance= (PricePerSack)getIntent().getSerializableExtra("pricepersackbundle");

        numberOfTrees.setText(String.valueOf(pricePerSackInstance.getPricePerSack_numberOfTrees()));
        numberOfFruits.setText(String.valueOf(pricePerSackInstance.getPricePerSack_numberOfCoconutl()));
        numberOfSack.setText(String.valueOf(pricePerSackInstance.getPricePerSack_numberOfSacks()));
        pricePersack.setText(String.valueOf(pricePerSackInstance.getPricePerSack_pricePerSack()));
    }

    public void saveUpdatedValues() {

        ProductsType productsType= new ProductsType(pricePerSackInstance.getId());

        int idVal,numOfTreesval, numOfFruitsVal, numOfSacksVal;
        double  pricePerSacksVal, initialIncome;
        String cocoVarietyVal, cocoSizeVal, cocoProductTypesVal;
        Date dateEdited;

        idVal=pricePerSackInstance.getId();
        numOfTreesval= Integer.parseInt(numberOfTrees.getText().toString());
        numOfFruitsVal= Integer.parseInt(numberOfFruits.getText().toString());
        numOfSacksVal= Integer.parseInt(numberOfSack.getText().toString());
        pricePerSacksVal= Double.parseDouble(pricePersack.getText().toString());
        cocoVarietyVal= cocoVariety.getSelectedItem().toString();
        cocoSizeVal= cocoSize.getSelectedItem().toString();
        cocoProductTypesVal= cocoProductsType.getSelectedItem().toString();
        dateEdited= SolidProductsActivity.getCurrentTimeStamp();

        initialIncome= numOfSacksVal*pricePerSacksVal;

        priceperSackObj= new PricePerSack(idVal,productsType,numOfTreesval,numOfFruitsVal, cocoVarietyVal,cocoSizeVal,
                cocoProductTypesVal,numOfSacksVal,pricePerSacksVal,initialIncome,dateEdited );

        try {
            pricePerSackIntegerDao.update(priceperSackObj);
            pricePerSackIntegerDao.refresh(priceperSackObj);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void setupCocoVarietySpinner() {
        ArrayAdapter cocoVarietyAdapter= ArrayAdapter.createFromResource(getBaseContext(), R.array.coconut_varieties, android.R.layout.simple_spinner_item);
        cocoVarietyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cocoVariety.setAdapter(cocoVarietyAdapter);
    }

    private void setupCocoSizeSpinner() {
        ArrayAdapter cocoSizeAdapter= ArrayAdapter.createFromResource(getBaseContext(), R.array.coconut_sizes, android.R.layout.simple_spinner_item);
        cocoSizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cocoSize.setAdapter(cocoSizeAdapter);
    }

    private void setupCocoProductsType() {
        ArrayAdapter cocoProductsTypeAdapter= ArrayAdapter.createFromResource(getBaseContext(), R.array.products_type, android.R.layout.simple_spinner_item);
        cocoProductsTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cocoProductsType.setAdapter(cocoProductsTypeAdapter);
    }

}
