package coconut.app.coconapp.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.query.Exists;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import coconut.app.coconapp.Adapters.LiquidProductsAdapter;
import coconut.app.coconapp.Classes.MonthlyDataRetrievalUtil;
import coconut.app.coconapp.Classes.PricePerGallon;
import coconut.app.coconapp.Classes.ProductsType;
import coconut.app.coconapp.Database.DatabaseHelper;
import coconut.app.coconapp.R;

public class LiquidProductsActivity extends AppCompatActivity{


    final static int PRICE_PER_GALLON_ID=3;
    Dao<PricePerGallon,Integer> pricePerGallonIntegerDao;
    PricePerGallon   pricePerGallonObj;
    DatabaseHelper dbHelper=null;
    EditText   numberOfTrees,numberOfCoconut,numberOfLiters,pricePerLiter,labor;
    Spinner  coconutVariety,coconutSize;
    RecyclerView liquidProducts;
    LiquidProductsAdapter liquidProductsAdapter;

    List<PricePerGallon> pricePerGallons,reversedPricePerGallons;

    MonthlyDataRetrievalUtil monthlyDataRetrievalUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liquid_products);

        try {
            pricePerGallonIntegerDao=getHelper().getPricePerGallonIntegerDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        Toolbar toolbar= (Toolbar)findViewById(R.id.liquidProducts_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        pricePerGallons= new ArrayList<>();

       try {
            pricePerGallons= pricePerGallonIntegerDao.queryForAll();
            reversedPricePerGallons= new ArrayList<>();
            Collections.reverse(pricePerGallons);
            reversedPricePerGallons= pricePerGallons;

        } catch (SQLException e) {
            e.printStackTrace();
        }


        liquidProductsAdapter= new LiquidProductsAdapter(reversedPricePerGallons);
        liquidProducts= (RecyclerView)findViewById(R.id.liquidproducts_recyclervew);
        liquidProducts.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getApplicationContext());
        liquidProducts.setLayoutManager(layoutManager);
        liquidProducts.setAdapter(liquidProductsAdapter);
        liquidProductsAdapter.notifyDataSetChanged();

        monthlyDataRetrievalUtil= new MonthlyDataRetrievalUtil(this);

        try {
            String[] total=  monthlyDataRetrievalUtil.getOverallIncomeInSingleQuarterPricePerGallon(MonthlyDataRetrievalUtil.JAN, MonthlyDataRetrievalUtil.MAR);
            Log.i("Total",total[0]);
        } catch (SQLException e) {
            e.printStackTrace();
        }catch (NullPointerException e) {
            e.printStackTrace();
           Log.i("Getting Overall Income","No Data!");
        }


    }

   /** Getting DBHelpers instance */
    private DatabaseHelper getHelper() {
        if (dbHelper==null) {
            dbHelper= OpenHelperManager.getHelper(this,DatabaseHelper.class);
        }

        return dbHelper;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         getMenuInflater().inflate(R.menu.liquidproducts_menu,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.addliquidproducts_menu) {
            createPricePerGallonsDialog();
        }if (item.getItemId() == android.R.id.home) {
            //Send intent to main activity
            sendIntentToMainActivity(pricePerGallonObj);
        }

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbHelper != null) {
            OpenHelperManager.releaseHelper();
            dbHelper=null;
        }
    }

    public void createPricePerGallonsDialog() {

        ArrayAdapter<CharSequence> cocoVarieryAdapter,cocoSizeAdapter;
      final   ProductsType productsType;

        productsType= new ProductsType(PRICE_PER_GALLON_ID);
       final AlertDialog.Builder builder= new AlertDialog.Builder(this);
        LayoutInflater inflater= this.getLayoutInflater();
       View customPricePerGallonsView= inflater.inflate(R.layout.priceper_gallons,null);
        builder.setView(customPricePerGallonsView);

        numberOfTrees= (EditText)customPricePerGallonsView.findViewById(R.id.pricerpergallon_numberoftrees_editext);
        numberOfCoconut=(EditText)customPricePerGallonsView.findViewById(R.id.pricerpergallon_numberofcoconut_editext);
        numberOfLiters=(EditText)customPricePerGallonsView.findViewById(R.id.pricepergallon_numberofliters_editext);
        pricePerLiter=(EditText)customPricePerGallonsView.findViewById(R.id.pricepergallon_priceperliter_editext);
        labor=(EditText)customPricePerGallonsView.findViewById(R.id.pricepergallon_labor_editext);

        coconutVariety=(Spinner)customPricePerGallonsView.findViewById(R.id.pricepergallon_coconutvariety_spinner);
        coconutSize= (Spinner)customPricePerGallonsView.findViewById(R.id.pricepergallon_coconutsize_spinner);

        // Setting Coconut Varieties adapter and spinner instance, also getting the choosed variety
        cocoVarieryAdapter= ArrayAdapter.createFromResource(getBaseContext(),R.array.coconut_varieties,android.R.layout.simple_spinner_item);
        cocoVarieryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        coconutVariety.setAdapter(cocoVarieryAdapter);

        cocoSizeAdapter= ArrayAdapter.createFromResource(getBaseContext(),R.array.coconut_sizes,android.R.layout.simple_spinner_item);
        cocoSizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        coconutSize.setAdapter(cocoSizeAdapter);

        builder.setTitle("Add Tuba product");

        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                try {

                    int  numberOfTreesValue= Integer.parseInt(numberOfTrees.getText().toString().trim());
                    int  numberOfCoconutValue= Integer.parseInt(numberOfCoconut.getText().toString().trim());
                    double    numberOfLitersValue= Double.parseDouble(numberOfLiters.getText().toString().trim());
                    double  pricePerLiterValue=Double.parseDouble(pricePerLiter.getText().toString().trim());
                    double  laborValue=Double.parseDouble(labor.getText().toString().trim());
                    String selectedCoconutSize=coconutSize.getSelectedItem().toString();
                    String  selectedVarietyValue=coconutVariety.getSelectedItem().toString();
                    Date dateCreated= getCurrentTimeStamp();

                    double initialIncome=(numberOfLitersValue*pricePerLiterValue)-laborValue;



                    pricePerGallonObj= new PricePerGallon(productsType,numberOfTreesValue,numberOfCoconutValue,selectedVarietyValue,selectedCoconutSize,numberOfLitersValue,pricePerLiterValue,laborValue,initialIncome,dateCreated);

                    pricePerGallonIntegerDao= getHelper().getPricePerGallonIntegerDao();
                    pricePerGallonIntegerDao.create(pricePerGallonObj);

                    if (reversedPricePerGallons != null) {

                        try {
                            reversedPricePerGallons= pricePerGallonIntegerDao.queryForAll();
                            Collections.reverse(reversedPricePerGallons);

                            liquidProductsAdapter= new LiquidProductsAdapter(reversedPricePerGallons);
                            liquidProducts.setAdapter(liquidProductsAdapter);
                            liquidProductsAdapter.notifyDataSetChanged();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }catch (NumberFormatException f) {
                    Log.i("NumberFormatException","Triggered");
                    f.printStackTrace();
                    Toast.makeText(getBaseContext(),"Invalid Values! Please check again.",Toast.LENGTH_LONG).show();
                }

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        AlertDialog alertDialog= builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }
    public static Date getCurrentTimeStamp() {

        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateProductCreated= new Date();
       sdf.format(dateProductCreated);
        return dateProductCreated;
    }

    @Override
    protected void onResume() {
        Log.i("LiquidProducts","OnResume");
        super.onResume();
     //   updateRecyclerView();
    }

    public void sendIntentToMainActivity(PricePerGallon pricePerGallon) {
        Intent i= new Intent();
        i.putExtra("value",pricePerGallon);
        setResult(RESULT_OK,i);
        finish();
    }
}
