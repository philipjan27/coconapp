package coconut.app.coconapp.Activities;

import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.LoginFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.field.types.FloatObjectType;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import coconut.app.coconapp.Adapters.PricePerKiloRecyclerViewAdapter;
import coconut.app.coconapp.Classes.MonthlyDataRetrievalUtil;
import coconut.app.coconapp.Classes.PricePerFruit;
import coconut.app.coconapp.Classes.PricePerGallon;
import coconut.app.coconapp.Classes.PricePerKilo;
import coconut.app.coconapp.Classes.PricePerSack;
import coconut.app.coconapp.Classes.ProductsType;
import coconut.app.coconapp.Database.DatabaseHelper;
import coconut.app.coconapp.Fragments.Priceperkilo;
import coconut.app.coconapp.R;

import static coconut.app.coconapp.R.id.cancel_action;
import static coconut.app.coconapp.R.id.end;
import static coconut.app.coconapp.R.id.textview_totalIncome;

public class MainActivity extends AppCompatActivity{

    public int REQUEST_CODE=1;
    DatabaseHelper dbHelper= null;
    Dao<ProductsType,Integer> productsTypedao;
    Dao<PricePerGallon,Integer> pricePerGallonIntegerDao;
    Dao<PricePerKilo,Integer> pricePerKiloIntegerDao;
    Dao<PricePerFruit,Integer> pricePerFruitIntegerDao;
    Dao<PricePerSack,Integer> pricePerSackIntegerDao;

    PieChart pieChart;

    Long  totalTrees, totalCoconuts;
    double pricePerGallonTotalIncome,pricePerKiloTotalIncome, pricePerFruitTotalIncome, pricePerSackTotalIncome,overAllIncome;
    TextView overAllTotalIncome,overallTotalTrees,overAllTotalCoconuts;
    PricePerGallon pricePerGallon;
    MonthlyDataRetrievalUtil monthlyDataRetrievalUtil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("onCreate","onCreate");
        setContentView(R.layout.activity_main);
        monthlyDataRetrievalUtil= new MonthlyDataRetrievalUtil(this);

        pieChart= (PieChart)findViewById(R.id.piechart_id);
        overAllTotalIncome= (TextView)findViewById(textview_totalIncome);
        overallTotalTrees= (TextView)findViewById(R.id.textview_totalNumberOfTrees);
        overAllTotalCoconuts= (TextView)findViewById(R.id.textview_totalNumberOfCoconuts);

        // Update coconut/trees overall values
        updateTreesAndCoconutsValues();


        /**
         * Displaying Pie Chart in onCreate
         * */
       try {
            displayPieChart();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            productsTypedao= getHelper().getProductsTypeDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        getOverallIncome();

        pricePerGallon= new PricePerGallon();

        Toolbar toolbar= (Toolbar)findViewById(R.id.mainactivity_toolbar);
        setSupportActionBar(toolbar);

        initializeAddingValues(productsTypedao);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            Log.d("RequestCode","1");
            if (resultCode == 1) {
                Log.d("ResultCode","1");

                getOverallIncome();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("onStart","onStart");

    }

    @Override
    protected void onResume() {
        super.onResume();

        getOverallIncome();
        updateTreesAndCoconutsValues();

        try {
            displayPieChart();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Log.i("onResume","onResume");

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("onRestart","onRestart");

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("onStop","onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbHelper != null) {
            OpenHelperManager.releaseHelper();
            dbHelper= null;
        }

        pieChart.clearValues();
        Log.i("onDestroy","onDestroy");
    }

    // To get instance
    private DatabaseHelper getHelper() {
        if (dbHelper==null) {
            dbHelper= OpenHelperManager.getHelper(this,DatabaseHelper.class);
        }
        return dbHelper;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         getMenuInflater().inflate(R.menu.mainactivity_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {

                case R.id.menu_solid_products:
                    startActivity(new Intent(MainActivity.this,SolidProductsActivity.class));
                    break;
                case R.id.menu_liquid_products:
                    startActivityForResult(new Intent(MainActivity.this,LiquidProductsActivity.class),REQUEST_CODE);
                    break;
                default:
                    break;
            }

        return false;
    }

    // Check if Values already in database
    public List<ProductsType> queryIfDataAlreadyExist(Dao<ProductsType,Integer> pDao) {

        List<ProductsType> productsTypeList= new ArrayList<>();
        try {
            productsTypeList = pDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return productsTypeList;
    }

    // Add default products
    public void addDefaultProducts () {

        try {

            productsTypedao.createIfNotExists(new ProductsType("Price per sack"));
            productsTypedao.createIfNotExists(new ProductsType("Price per fruit"));
            productsTypedao.createIfNotExists(new ProductsType("Price per gallon"));
            productsTypedao.createIfNotExists(new ProductsType("Price per kilo"));

            Log.i("Products Type",String.valueOf(productsTypedao));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Initialize chekcing of values if empty or not
    public void initializeAddingValues(Dao<ProductsType,Integer> pDao) {
        // If is not empty, do nothing
        if (!queryIfDataAlreadyExist(pDao).isEmpty()) {
            Log.i(LiquidProductsActivity.class.getName(),"Already have values!");
        }else {
            addDefaultProducts();
        }
    }


    /** Method to update textview once new item is added */

    public void updateTreesAndCoconutsValues() {

        // Setting total number of trees
        totalTrees= getAllTotalTrees();
        Log.i("TOTAL TREES",totalTrees.toString());
        overallTotalTrees.setText(String.valueOf(totalTrees));

        // Setting total number of coconuts
        totalCoconuts= getAllTotalCoconuts();
        overAllTotalCoconuts.setText(String.valueOf(totalCoconuts));
        Log.i("TOTAL COCONUTS",String.valueOf(totalCoconuts));

    }

    /**
     * Getting overall total income price per sack
     * */

    public double getTotalIncomePricePerSack() {
        double total= 0.0;

        try {
            pricePerSackIntegerDao= getHelper().getPricePerSackIntegerDao();
            GenericRawResults<String[]> rawResults= pricePerSackIntegerDao.queryRaw("select sum(initial_income) from tbl_productspersack_data");
            List<String[]> result= rawResults.getResults();

            String[] value= result.get(0);

            total= Double.parseDouble(value[0]);
        }catch (SQLException e) {
            e.printStackTrace();
        }catch (NullPointerException f) {
            total=0.0;
        }

        return total;
    }

    /**
     * Getting overall total income price per kilo
     * */
    public double getTotalIncomePricePerKilo() {
        double total= 0.0;

        try {
            pricePerKiloIntegerDao= getHelper().getPricePerKiloIntegerDao();
            GenericRawResults<String[]> rawResults= pricePerKiloIntegerDao.queryRaw("select sum(initial_income) from tbl_productsperkilo_data");
            List<String[]> results= rawResults.getResults();

            String[] value=results.get(0);
            total= Double.parseDouble(value[0]);

        } catch (SQLException e) {
            e.printStackTrace();
        }catch (NullPointerException f) {
            total=0.0;
        }

        return total;
    }


    /**
     * Getting total income priceper fruit
     * @return
     */

     public double getTotalIncomePricePerFruit() {
         double total= 0.0;

        try {
            pricePerFruitIntegerDao= getHelper().getPricePerFruitDao();
            GenericRawResults<String[]> rawResults= pricePerFruitIntegerDao.queryRaw("select sum(initial_income) from tbl_productsperfruit_data");
            List<String[]> results= rawResults.getResults();

            String[] resultArray= results.get(0);
           total = Double.parseDouble(resultArray[0]);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException f){
            total=0.0;
        }
        return total;
    }

    /**
     *  Getting Total values in PricePerGallon
     */

    public double getTotalIncomePricePerGallon() {

        double total=0.0;
        try {
            pricePerGallonIntegerDao= getHelper().getPricePerGallonIntegerDao();
            GenericRawResults<String[]> rawResults= pricePerGallonIntegerDao.queryRaw("select sum(initial_income) from tbl_pricepergallon_data");

            List<String[]> results= rawResults.getResults();

            String[] resultArray= results.get(0);
            total= Double.parseDouble(resultArray[0]);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException f) {
            total=0.0;
        }

        return  total;

    }

    /** Getting Total values of all products to display in TextView */
   public void getAllTotalIncomesInAllProducts() {


       pricePerGallonTotalIncome= getTotalIncomePricePerGallon();
      /**  try {
            pricePerGallonIntegerDao=getHelper().getPricePerGallonIntegerDao();
            pricePerGallonTotalIncome= pricePerGallonIntegerDao.queryRawValue("select sum(initial_income) from tbl_pricepergallon_data");
            Log.i("PricePerGallon",String.valueOf(pricePerGallonTotalIncome));
        } catch (SQLException e) {
            e.printStackTrace();
        } */

       pricePerFruitTotalIncome= getTotalIncomePricePerFruit();
      /**  try {
            pricePerFruitIntegerDao= getHelper().getPricePerFruitDao();
            pricePerFruitTotalIncome= pricePerFruitIntegerDao.queryRawValue("select sum(initial_income) from tbl_productsperfruit_data");
            Log.i("PricePerFruit",String.valueOf(pricePerFruitTotalIncome));
        } catch (SQLException e) {
            e.printStackTrace();
        } */

       pricePerKiloTotalIncome= getTotalIncomePricePerKilo();
       /** try {
            pricePerKiloIntegerDao= getHelper().getPricePerKiloIntegerDao();
            pricePerKiloTotalIncome= pricePerKiloIntegerDao.queryRawValue("select sum(initial_income) from tbl_productsperkilo_data");
            Log.i("PricePerKilo",String.valueOf(pricePerKiloTotalIncome));
        } catch (SQLException e) {
            e.printStackTrace();
        } */


       pricePerSackTotalIncome= getTotalIncomePricePerSack();
       /** try {
            pricePerSackIntegerDao= getHelper().getPricePerSackIntegerDao();
            pricePerSackTotalIncome= pricePerSackIntegerDao.queryRawValue("select sum(initial_income) from tbl_productspersack_data");
            Log.i("PricePerSack",String.valueOf(pricePerSackTotalIncome));
        } catch (SQLException e) {
            e.printStackTrace();
        } */

       /**
        * Adding all  incomes
        * */


        overAllIncome= pricePerFruitTotalIncome+pricePerKiloTotalIncome+pricePerSackTotalIncome+pricePerGallonTotalIncome;

       /**
        * Getting Highest Values of all 4 products
        * */
      double max= Math.max(Math.max(pricePerFruitTotalIncome,pricePerKiloTotalIncome),Math.max(pricePerSackTotalIncome,pricePerGallonTotalIncome));
       double min= Math.min(Math.min(pricePerFruitTotalIncome,pricePerKiloTotalIncome),Math.min(pricePerSackTotalIncome,pricePerGallonTotalIncome));


        if (max == pricePerFruitTotalIncome) {
            Toast.makeText(this,"As per "+getCurrentDateTime()+", highest income is Price Per Fruit",Toast.LENGTH_SHORT).show();
        }else if (max == pricePerKiloTotalIncome) {
            Toast.makeText(this,"As per "+getCurrentDateTime()+", highest income is Price Per Kilo",Toast.LENGTH_SHORT).show();
        }else if (max == pricePerSackTotalIncome) {
            Toast.makeText(this,"As per "+getCurrentDateTime()+", highest income is Price Per Sack",Toast.LENGTH_SHORT).show();
        }else if (max == pricePerGallonTotalIncome) {
            Toast.makeText(this,"As per "+getCurrentDateTime()+", highest income is Tuba",Toast.LENGTH_SHORT).show();
        }


       if (min == pricePerFruitTotalIncome) {
           Toast.makeText(this,"As per "+getCurrentDateTime()+", lowest income is Price Per Fruit",Toast.LENGTH_SHORT).show();
       }else if (min == pricePerKiloTotalIncome) {
           Toast.makeText(this,"As per "+getCurrentDateTime()+", lowest income is Price Per Kilo",Toast.LENGTH_SHORT).show();
       }else if (min == pricePerSackTotalIncome) {
           Toast.makeText(this,"As per "+getCurrentDateTime()+", lowest income is Price Per Sack",Toast.LENGTH_SHORT).show();
       }else if (min == pricePerGallonTotalIncome) {
           Toast.makeText(this,"As per "+getCurrentDateTime()+", lowest income is Tuba",Toast.LENGTH_SHORT).show();
       }

    }

    /**
     * Getting all total coconut trees
     * */
    public long getAllTotalCoconuts() {
        long totalCoconuts=0;
        long pricePerKiloTotalCoconuts=0;
        long pricePerSackTotalCoconuts=0;
        long pricePerFruitTotalCoconuts=0;
        long pricePerLiquidTotalCoconuts=0;

        try {
            pricePerKiloIntegerDao= getHelper().getPricePerKiloIntegerDao();
            pricePerKiloTotalCoconuts= pricePerKiloIntegerDao.queryRawValue("select sum(numberof_coconut) from tbl_productsperkilo_data");
        }catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            pricePerSackIntegerDao= getHelper().getPricePerSackIntegerDao();
            pricePerSackTotalCoconuts= pricePerSackIntegerDao.queryRawValue("select sum(numberof_coconut) from tbl_productspersack_data");
        }catch (SQLException e) {
            e.printStackTrace();
        }

        try{
            pricePerFruitIntegerDao= getHelper().getPricePerFruitDao();
            pricePerFruitTotalCoconuts= pricePerFruitIntegerDao.queryRawValue("select sum(numberof_coconuts) from tbl_productsperfruit_data");
        }catch (SQLException e){
            e.printStackTrace();
        }

        try {
            pricePerGallonIntegerDao= getHelper().getPricePerGallonIntegerDao();
            pricePerLiquidTotalCoconuts= pricePerGallonIntegerDao.queryRawValue("select sum(numberof_coconut) from tbl_pricepergallon_data");
        }catch (SQLException e) {
            e.printStackTrace();
        }

        totalCoconuts= pricePerFruitTotalCoconuts+pricePerKiloTotalCoconuts+pricePerLiquidTotalCoconuts+pricePerSackTotalCoconuts;

        return totalCoconuts;
    }



    /**
     * Getting all total trees
     * */
    public long getAllTotalTrees() {

        long totalTrees=0;
        long pricePerKiloTotalTrees=0;
        long pricePerSackTotalTrees=0;
        long pricePerFruitTotalTrees=0;
        long pricePerLiquidTotalTrees=0;


        try {
            pricePerFruitIntegerDao= getHelper().getPricePerFruitDao();
            pricePerFruitTotalTrees= pricePerFruitIntegerDao.queryRawValue("select sum(numberof_trees) from tbl_productsperfruit_data");
        } catch (SQLException e) {
            e.printStackTrace();
        }


            try {
                pricePerKiloIntegerDao= getHelper().getPricePerKiloIntegerDao();
                pricePerKiloTotalTrees= pricePerKiloIntegerDao.queryRawValue("select sum(numberof_trees) from tbl_productsperkilo_data");
            } catch (SQLException e) {
                e.printStackTrace();
            }

        try {
            pricePerSackIntegerDao= getHelper().getPricePerSackIntegerDao();
            pricePerSackTotalTrees= pricePerSackIntegerDao.queryRawValue("select sum(numberof_trees) from tbl_productspersack_data");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            pricePerGallonIntegerDao=getHelper().getPricePerGallonIntegerDao();
            pricePerLiquidTotalTrees= pricePerGallonIntegerDao.queryRawValue("select sum(numberof_tree) from tbl_pricepergallon_data");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        totalTrees= pricePerFruitTotalTrees+pricePerKiloTotalTrees+pricePerSackTotalTrees+pricePerLiquidTotalTrees;

        return totalTrees;

    }

    /**
     * Reset the adapter to display new values
     * */
    public void getOverallIncome() {
       getAllTotalIncomesInAllProducts();
        overAllTotalIncome.setText("₱"+String.valueOf(overAllIncome));
    }

    /**
     * Display datas in Pie Chart
     * */
    public void displayPieChart() throws SQLException {

        ArrayList<String> monthsQuarter= new ArrayList<>();

        monthsQuarter.add("Copra");
        monthsQuarter.add("Uling/Bunot");
        monthsQuarter.add("Bunga");
        monthsQuarter.add("Tuba");

        String currentMonth= MonthlyDataRetrievalUtil.getCurrentMonth();

        switch (currentMonth) {

            case "01":
            case "02":
            case "03":
                displayChartData(monthsQuarter,MonthlyDataRetrievalUtil.JAN, MonthlyDataRetrievalUtil.MAR, "1st Quarter");
                // Code to Display Bar Chart in Jan/Feb/march
                break;
            case "04":
            case "05":
            case "06":
                displayChartData(monthsQuarter, MonthlyDataRetrievalUtil.APR, MonthlyDataRetrievalUtil.JUN,"2nd Quarter");
                // Code to Display in Bar Chart in April/May/Jun
                break;
            case "07":
            case "08":
            case "09":
                displayChartData(monthsQuarter, MonthlyDataRetrievalUtil.JUL, MonthlyDataRetrievalUtil.SEP,"3rd Quarter");
                // Code to Display in Bar Chart in July/August/September
                break;
            case "10":
            case "11":
            case "12":
                displayChartData(monthsQuarter, MonthlyDataRetrievalUtil.OCT, MonthlyDataRetrievalUtil.DEC,"4th Quarter");
                // Code to Display in Bar Chart in October/November/December
                break;
            default:
                Log.i("CurrentMonth","None of the choices");
        }

    }

    public void displayChartData(ArrayList<String> monthQuarters,String startMonth, String endMonth,String quarter) throws SQLException {

        List<Entry> quarterTotalIncome=null;
        quarterTotalIncome= new ArrayList<>();

        String[] pricePerKiloQuarterTotal= monthlyDataRetrievalUtil.getOverallIncomeInSingleQuarterPricePerKilo(startMonth,endMonth);
        String[] pricePerSackQuarterTotal= monthlyDataRetrievalUtil.getOverAllIncomeInSingleQuarterPricePerSack(startMonth,endMonth);
        String[] pricePerFruitQuarterTotal=monthlyDataRetrievalUtil.getOverAllIncomeInSingleQuarterPricePerFruit(startMonth,endMonth);
        String[] pricePerGallonQuarterTotal=monthlyDataRetrievalUtil.getOverallIncomeInSingleQuarterPricePerGallon(startMonth,endMonth);


        // Getting products quarterly income
       try {
           quarterTotalIncome.add(new Entry(Float.parseFloat(pricePerKiloQuarterTotal[0]),0));
           quarterTotalIncome.add(new Entry(Float.parseFloat(pricePerSackQuarterTotal[0]),1));
           quarterTotalIncome.add(new Entry(Float.parseFloat(pricePerFruitQuarterTotal[0]),2));
           quarterTotalIncome.add(new Entry(Float.parseFloat(pricePerGallonQuarterTotal[0]),3));
       }catch (NullPointerException e) {
           e.printStackTrace();
           Log.e("displayChartData","Empty Values");
       }

        if (convertArraysToFloats(pricePerKiloQuarterTotal,pricePerFruitQuarterTotal,pricePerGallonQuarterTotal, pricePerSackQuarterTotal)) {
            displaySingleValuedPieChart();
        }else {

            Legend l= pieChart.getLegend();
            l.setTextSize(10f);
            l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART_CENTER);
            l.setYEntrySpace(4);

            PieDataSet pieDataSet= new PieDataSet(quarterTotalIncome, quarter);
            pieDataSet.setColors(ColorTemplate.PASTEL_COLORS);
            pieDataSet.setSliceSpace(3);

            PieData pieData= new PieData(monthQuarters,pieDataSet);
            pieData.setValueTextSize(9.5f);

            pieChart.setDescription("");
            pieChart.setDrawHoleEnabled(false);
            pieChart.setData(pieData);
            pieChart.invalidate();
            pieChart.notifyDataSetChanged();
        }



    }
    /**
     * Default display of PieChart if Empty
     * */
    private void displaySingleValuedPieChart() {

        List<String> xValues=null;
        List<Entry> singleEntryList=null;


        singleEntryList= new ArrayList<>();
        xValues= new ArrayList<>();
        xValues.add("Empty Chart");
        singleEntryList.add(new Entry(1f,0));

        Legend singleValueLegend= pieChart.getLegend();
        singleValueLegend.setPosition(Legend.LegendPosition.RIGHT_OF_CHART_CENTER);
        PieDataSet singleValuePieDataSet= new PieDataSet(singleEntryList,"");
        singleValuePieDataSet.setDrawValues(false);
        singleValuePieDataSet.setColors(ColorTemplate.PASTEL_COLORS);

        PieData singleValuesPieData= new PieData(xValues,singleValuePieDataSet);
        pieChart.setDescription("");
        pieChart.setDrawHoleEnabled(false);
        pieChart.setData(singleValuesPieData);
        pieChart.animateX(5000);
        pieChart.notifyDataSetChanged();
    }

    /**
     * Check if all product items are empty
     * */
    private boolean convertArraysToFloats(String[] ppk, String[] ppf, String[] ppg, String[] pps) {
        boolean isEmpty= true;
        Float ppkObj=null;
        Float ppfObj=null;
        Float ppgObj=null;
        Float ppsObj=null;

        try {
           ppkObj= Float.parseFloat(ppk[0]);

           ppfObj= Float.parseFloat(ppf[0]);

            ppgObj= Float.parseFloat(ppg[0]);

            ppsObj= Float.parseFloat(pps[0]);

        }catch (NullPointerException e) {
            e.printStackTrace();
            Log.e("convertArraysToFloats","Null Values");
        }

        if (ppkObj == null && ppfObj == null && ppgObj == null && ppsObj == null ) {
            return isEmpty;
        }

        return false;
    }

    private String getCurrentDateTime() {
        DateFormat sdf= new SimpleDateFormat("MMM dd, yyyy");
        Date date= new Date();
        String currentdate= sdf.format(date);
        return currentdate;
    }
}
