package coconut.app.coconapp.Activities;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.security.PublicKey;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import coconut.app.coconapp.Adapters.PricePerKiloRecyclerViewAdapter;
import coconut.app.coconapp.Classes.PricePerFruit;
import coconut.app.coconapp.Classes.PricePerKilo;
import coconut.app.coconapp.Classes.PricePerSack;
import coconut.app.coconapp.Classes.ProductsType;
import coconut.app.coconapp.Database.DatabaseHelper;
import coconut.app.coconapp.Fragments.Priceperkilo;
import coconut.app.coconapp.R;

public class SolidProductsActivity extends AppCompatActivity {


                private final int PRICE_PER_SACK=1;
                private final int PRICE_PER_FRUIT=2;
                private final int PRICE_PER_KILO=4;

                ViewPager viewPager;
                List<PricePerKilo> priceperkilosList=null;
                DatabaseHelper helper= null;
                Dao<PricePerFruit,Integer> pricePerFruitIntegerDao;
                Dao<PricePerKilo,Integer> pricePerKiloIntegerDao;
                Dao<PricePerSack,Integer> pricePerSackIntegerDao;
                ProductsType productsType;

                /**
                 *
                 * Price per kilo Widgets
                 *
                 * */
                EditText numberOfTrees,numberOfCoconuts,numberOfSacks,pricerPerKilo,initialKilo,labor;
                Spinner cocoVariety,coconutSize;

                /**
                 *
                 * Price per fruit Widgets
                 *
                 */
                EditText spNumberOfTree, spNumberOfCoconut, spPricePerFruit,spLabor;
                Spinner spFruitSizeSpinner, spFruitVariety;
                /**
                 *
                 * Price per sack Widgets
                 *
                 */
                EditText ppsNumberOfTree, numberOfFruits, numberOfSack,priceperSack;
                Spinner ppsCoconutVariety,ppsFruitsSize, ppsProductType;

                @Override
                protected void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    setContentView(R.layout.activity_solid_products);


                    // Setting toolbar as default actionbar
                    Toolbar toolbar= (Toolbar)findViewById(R.id.solidproducts_toolbar);
                    setSupportActionBar(toolbar);
                    getSupportActionBar().setElevation(0);

                    TabLayout tabLayout= (TabLayout)findViewById(R.id.tab_layout);

                    // Adding Tabs
                    tabLayout.addTab(tabLayout.newTab().setText("Copra"));
                    tabLayout.addTab(tabLayout.newTab().setText("Uling/Bunot"));
                    tabLayout.addTab(tabLayout.newTab().setText("Bunga"));
                    tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);



                    viewPager= (ViewPager)findViewById(R.id.viewpager_tab);
                    PagerAdapter pagerAdapter= new coconut.app.coconapp.Adapters.PagerAdapter(getSupportFragmentManager(),tabLayout.getTabCount());

                    // Setting viewpagers adapter
                    viewPager.setAdapter(pagerAdapter);

                    // On Page Change Listener
                    viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

                    // On tab selected listener=
                    tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                        @Override
                        public void onTabSelected(TabLayout.Tab tab) {
                            viewPager.setCurrentItem(tab.getPosition());
                        }

                        @Override
                        public void onTabUnselected(TabLayout.Tab tab) {

                        }

                        @Override
                        public void onTabReselected(TabLayout.Tab tab) {
                            viewPager.getAdapter().notifyDataSetChanged();
                        }
                    });


                }

                // To get instance
                private DatabaseHelper getHelper() {
                    if (helper==null) {
                        helper= OpenHelperManager.getHelper(this,DatabaseHelper.class);
                    }

                    return helper;
                }

                @Override
                public boolean onCreateOptionsMenu(Menu menu) {
                    getMenuInflater().inflate(R.menu.menu_main,menu);


                    return true;
                }

                @Override
                public boolean onOptionsItemSelected(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.addproduct_priceperkilo:
                            createPricePerKiloDialog();
                            viewPager.getAdapter().notifyDataSetChanged();
                            break;
                        case R.id.addproduct_priceperfruits:
                            createPricePerFruitDialog();
                            break;
                        case  R.id.addproduct_pricepersack:
                createPricePerSackDialog();
                break;
            default:
                break;
        }

        return false;
    }

    /**
     * Price Per Kilo Dialog
     * */
    private void createPricePerKiloDialog() {

        priceperkilosList=null;
        ArrayAdapter<CharSequence> cocoVarietyAdapter, cocoSizeAdapter;

        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        LayoutInflater inflater= this.getLayoutInflater();
        final View customDialogView= inflater.inflate(R.layout.priceper_kilo,null);
        builder.setView(customDialogView);

        numberOfTrees= (EditText)customDialogView.findViewById(R.id.priceperkilo_editext_numberoftrees);
        numberOfCoconuts= (EditText)customDialogView.findViewById(R.id.priceperkilo_editext_numberoffruits);
        initialKilo=(EditText)customDialogView.findViewById(R.id.priceperkilo_editext_initialproductkilo);
        numberOfSacks=(EditText)customDialogView.findViewById(R.id.priceperkilo_editext_numberofsacks);
        pricerPerKilo=(EditText)customDialogView.findViewById(R.id.priceperkilo_editext_initialpriceperkilo);
        labor=(EditText)customDialogView.findViewById(R.id.priceperkilo_labor_editext);
        cocoVariety= (Spinner)customDialogView.findViewById(R.id.priceperkilo_cocovariety_spinner);
        coconutSize= (Spinner)customDialogView.findViewById(R.id.priceperkilo_cocosize_spinner);


        cocoVarietyAdapter= ArrayAdapter.createFromResource(this,R.array.coconut_varieties,android.R.layout.simple_spinner_item);
        cocoVarietyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cocoVariety.setAdapter(cocoVarietyAdapter);

        cocoSizeAdapter=ArrayAdapter.createFromResource(this,R.array.coconut_sizes,android.R.layout.simple_spinner_item);
        cocoSizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        coconutSize.setAdapter(cocoSizeAdapter);

        builder.setTitle("Add Copra Product");
        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {

                    int numOfTreesValue=Integer.parseInt(numberOfTrees.getText().toString());
                    int numOfCoconutsValue= Integer.parseInt(numberOfCoconuts.getText().toString());
                    String selectedVariety= cocoVariety.getSelectedItem().toString();
                    String selectedSize= coconutSize.getSelectedItem().toString();
                    int numOfSacksValue= Integer.parseInt(numberOfSacks.getText().toString());
                    double initialKiloValue= Double.parseDouble(initialKilo.getText().toString());
                    double pricePerkiloValue= Double.parseDouble(pricerPerKilo.getText().toString());
                    double laborValue= Double.parseDouble(labor.getText().toString());
                    Date dateCreated= getCurrentTimeStamp();

                    double initialIncome= (initialKiloValue*pricePerkiloValue)-laborValue;

                    productsType= new ProductsType(PRICE_PER_KILO);

                    PricePerKilo pricePerKilo= new PricePerKilo(productsType,numOfTreesValue,numOfCoconutsValue,selectedVariety,selectedSize,numOfSacksValue,initialKiloValue,pricePerkiloValue,laborValue,initialIncome,dateCreated);

                    pricePerKiloIntegerDao=getHelper().getPricePerKiloIntegerDao();

                    pricePerKiloIntegerDao.create(pricePerKilo);


                        } catch (SQLException e) {
                            e.printStackTrace();
                        }catch (NumberFormatException f) {
                    Log.e("NumberFormatException","Null");
                    Toast.makeText(getBaseContext(),"Please provide data's.",Toast.LENGTH_SHORT).show();
                }


            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog= builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        viewPager.getAdapter().notifyDataSetChanged();

    }

    /**
     * Pricer per Sack
     * */
    private void createPricePerSackDialog() {

        ArrayAdapter<CharSequence> ppsCoconutVarietyAdapter, ppsCoconutSizeAdapter, ppsProductTypeAdapter;

        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        LayoutInflater inflater= this.getLayoutInflater();
        final  View customPricepersackView= inflater.inflate(R.layout.priceper_sack,null);
        builder.setView(customPricepersackView);

        ppsNumberOfTree= (EditText)customPricepersackView.findViewById(R.id.pricepersack_numberoftrees);
        numberOfFruits= (EditText)customPricepersackView.findViewById(R.id.pricepersack_numberoffruits);
        numberOfSack= (EditText)customPricepersackView.findViewById(R.id.pricepersack_numberofsac_editext);
        priceperSack= (EditText)customPricepersackView.findViewById(R.id.pricepersack_pricepersack_editext);

        ppsCoconutVariety= (Spinner)customPricepersackView.findViewById(R.id.pricepersack_coconutVariety_spinner);
        ppsFruitsSize= (Spinner)customPricepersackView.findViewById(R.id.pricepersack_fruitsize_spinner);
        ppsProductType= (Spinner)customPricepersackView.findViewById(R.id.pricepersack_productType_spinner);


        ppsCoconutVarietyAdapter= ArrayAdapter.createFromResource(this,R.array.coconut_varieties, android.R.layout.simple_spinner_item);
        ppsCoconutVarietyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ppsCoconutVariety.setAdapter(ppsCoconutVarietyAdapter);

        ppsCoconutSizeAdapter= ArrayAdapter.createFromResource(this, R.array.coconut_sizes,android.R.layout.simple_spinner_item);
        ppsCoconutSizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ppsFruitsSize.setAdapter(ppsCoconutSizeAdapter);

        ppsProductTypeAdapter= ArrayAdapter.createFromResource(this, R.array.products_type,android.R.layout.simple_spinner_item);
        ppsProductTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ppsProductType.setAdapter(ppsProductTypeAdapter);

        builder.setTitle("Add Uling/Bunot Product");
        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                int numberOfTreesValue= Integer.parseInt(ppsNumberOfTree.getText().toString());
                int numberOfFruitsValue= Integer.parseInt(numberOfFruits.getText().toString());
                int numberOfSackValue= Integer.parseInt(numberOfSack.getText().toString());
                double pricePersackValue= Double.parseDouble(priceperSack.getText().toString());

                String choosedVariety= ppsCoconutVariety.getSelectedItem().toString();
                String choosedFruitSize=ppsFruitsSize.getSelectedItem().toString();
                String choosedProductType= ppsProductType.getSelectedItem().toString();
                Date dateCreated= getCurrentTimeStamp();
                double initialIncome= numberOfSackValue*pricePersackValue;

                ProductsType productsType= new ProductsType(PRICE_PER_SACK);
                PricePerSack pricePerSack= new PricePerSack(productsType,numberOfTreesValue,numberOfFruitsValue,choosedVariety,choosedFruitSize,choosedProductType,numberOfSackValue,pricePersackValue,initialIncome,dateCreated);
                try {
                    pricePerSackIntegerDao= getHelper().getPricePerSackIntegerDao();
                    pricePerSackIntegerDao.create(pricePerSack);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog= builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    /** Price per fruit Dialog */
    private void createPricePerFruitDialog() {

        ArrayAdapter<CharSequence> spinnerAdapter, spinnerAdapterCoconutProducts, spinnerAdapterCoconutVariety;

        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        LayoutInflater inflater= this.getLayoutInflater();
        final View customPricePerFruitsView= inflater.inflate(R.layout.priceper_fruit,null);

        spNumberOfTree= (EditText)customPricePerFruitsView.findViewById(R.id.priceperfruit_numberof_tree);
        spNumberOfCoconut= (EditText)customPricePerFruitsView.findViewById(R.id.priceperfruit_numberofcoconut);
        spPricePerFruit= (EditText)customPricePerFruitsView.findViewById(R.id.priceperfruit_priceperfruit_editext);
        spLabor= (EditText)customPricePerFruitsView.findViewById(R.id.priceperfruit_labor_editext);
        spFruitSizeSpinner= (Spinner)customPricePerFruitsView.findViewById(R.id.priceperfruit_fruitsize_spinner) ;
        spFruitVariety= (Spinner)customPricePerFruitsView.findViewById(R.id.priceperfruit_fruitvariety_spinner);


      // Spinner , Adapter, Array of sizes
        spinnerAdapter= ArrayAdapter.createFromResource(this,R.array.coconut_sizes,android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFruitSizeSpinner.setAdapter(spinnerAdapter);

        // Spinner Adapter, Variety
        spinnerAdapterCoconutVariety= ArrayAdapter.createFromResource(this,R.array.coconut_varieties,android.R.layout.simple_spinner_item);
        spinnerAdapterCoconutVariety.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFruitVariety.setAdapter(spinnerAdapterCoconutVariety);

        builder.setView(customPricePerFruitsView);
        builder.setTitle("Add Bunga Product");
        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                int numberOfTreesValue= Integer.parseInt(spNumberOfTree.getText().toString());
                int numberOfCoconutValue= Integer.parseInt(spNumberOfCoconut.getText().toString());
                double pricePerFruitValue= Double.parseDouble(spPricePerFruit.getText().toString());
                double laborValue= Double.parseDouble(spLabor.getText().toString());
                String choosedFruitSize= spFruitSizeSpinner.getSelectedItem().toString();
                String choosedFruitVariety= spFruitVariety.getSelectedItem().toString();
                Date dateCreated= getCurrentTimeStamp();
                double initialIncome= (numberOfCoconutValue*pricePerFruitValue)-laborValue;

                ProductsType productsType= new ProductsType(PRICE_PER_FRUIT);
                PricePerFruit pricePerFruit= new PricePerFruit(productsType,numberOfTreesValue,numberOfCoconutValue,choosedFruitVariety,choosedFruitSize,pricePerFruitValue,laborValue,initialIncome,dateCreated);
                try {
                    pricePerFruitIntegerDao= getHelper().getPricePerFruitDao();
                    pricePerFruitIntegerDao.create(pricePerFruit);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog= builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

    }

    public static Date getCurrentTimeStamp() {

        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateProductCreated= new Date();
        sdf.format(dateProductCreated);
        return dateProductCreated;
    }

}
