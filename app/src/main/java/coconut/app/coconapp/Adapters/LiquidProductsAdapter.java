package coconut.app.coconapp.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import coconut.app.coconapp.Activities.EditLiquidProductsActivity;
import coconut.app.coconapp.Classes.PricePerGallon;
import coconut.app.coconapp.Database.DatabaseHelper;
import coconut.app.coconapp.R;

/**
 * Created by EasyBreezy on 1/25/2017.
 */

public class LiquidProductsAdapter extends RecyclerView.Adapter<LiquidProductsAdapter.MyViewHolder> {


    List<PricePerGallon> itemList;
    String[] choices = {"Edit", "Delete"};


    DatabaseHelper helper = null;
    Dao<PricePerGallon, Integer> pricePerGallonIntegerDao;
    PricePerGallon pricePerGallon,intentObject, pricePerGallonToBeDeleted;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView numberOfTrees, numberOfCoconut, varietyOfCoconut, coconutSize, liters, pricePerLiter, laborPrice, dateCreated, initialIncome;

        public MyViewHolder(final View itemView) {
            super(itemView);

            numberOfTrees = (TextView) itemView.findViewById(R.id.numberoftrees_textview);
            numberOfCoconut = (TextView) itemView.findViewById(R.id.numberofcoconut_textview);
            varietyOfCoconut = (TextView) itemView.findViewById(R.id.varietyofcoconut_textview);
            coconutSize = (TextView) itemView.findViewById(R.id.sizeofcoconut_textview);
            liters = (TextView) itemView.findViewById(R.id.juiceliters_textview);
            pricePerLiter = (TextView) itemView.findViewById(R.id.priceperliter_temp_textview);
            laborPrice = (TextView) itemView.findViewById(R.id.laborPrice_textview);
            dateCreated = (TextView) itemView.findViewById(R.id.datecreated_textview);
            initialIncome = (TextView) itemView.findViewById(R.id.initialincome_textview);


            /** Item LongClick Events */
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View view) {

                  /**  Get the List Current Position */
                    final int idPosition = getLayoutPosition();
                    pricePerGallon = new PricePerGallon(idPosition);

                    final Context context = view.getContext();
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Options");
                    builder.setItems(choices, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            if (choices[i].contentEquals("Edit")) {
                                Log.i("Choosed", choices[i].toString());

                                /** Send data to the LiquidProductsEdit */
                                intentObject= new PricePerGallon();
                               intentObject= getItemInsideArraylist(itemList,idPosition);
                                Intent intent= new Intent(context,EditLiquidProductsActivity.class);
                                intent.putExtra("object",intentObject);
                                context.startActivity(intent);


                            }
                            if (choices[i].contentEquals("Delete")) {
                                Log.i("Choosed", choices[i].toString());
                                deleteLiquidProductDialog(view, getAdapterPosition());
                                notifyDataSetChanged();


                            }
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                    dialog.setCanceledOnTouchOutside(false);


                    Log.i("LongClick Event", "LongClicked position " + String.valueOf(idPosition));
                    return true;
                }
            });
        }
    }

    public LiquidProductsAdapter(List<PricePerGallon> itemList) {
        this.itemList = itemList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pricepergallon_customlayout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        /** Getting Item Position */
         pricePerGallon = itemList.get(position);

        holder.numberOfTrees.setText(String.valueOf(pricePerGallon.getPricePerGallon_numberOfTree()));
        holder.numberOfCoconut.setText(String.valueOf(pricePerGallon.getPricePerGallon_numberOfCoconut()));
        holder.varietyOfCoconut.setText(pricePerGallon.getPricerPerGallon_coconutVariety());
        holder.coconutSize.setText(pricePerGallon.getPricePerGallon_coconutSize());
        holder.liters.setText(String.valueOf(pricePerGallon.getPricePerGallon_numberOfLiters()));
        holder.laborPrice.setText(String.valueOf(pricePerGallon.getPricePerGallon_laborPrice()));
        holder.dateCreated.setText(String.valueOf(convertSqliteDateTime(pricePerGallon)));
        holder.initialIncome.setText(String.valueOf(pricePerGallon.getPricerPerGallon_initialIncome()));
        holder.pricePerLiter.setText(String.valueOf(pricePerGallon.getPricePerGallon_pricePerLiter()));
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void deleteLiquidProductDialog(final View v, final int position) {

        pricePerGallonToBeDeleted= new PricePerGallon();
        pricePerGallonToBeDeleted= getItemInsideArraylist(itemList,position);

        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setMessage("Sure to delete?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                helper = OpenHelperManager.getHelper(v.getContext(), DatabaseHelper.class);

                try {
                    pricePerGallonIntegerDao = helper.getPricePerGallonIntegerDao();
                   int result= pricePerGallonIntegerDao.delete(pricePerGallonToBeDeleted);
                    itemList.remove(position);
                    pricePerGallonIntegerDao.refresh(pricePerGallonToBeDeleted);
                    notifyDataSetChanged();

                    Log.i("Item Deletion",String.valueOf(result));

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**
     * Conversion of DateTime to readable formats
     * */
    public String convertSqliteDateTime(PricePerGallon pricePerGallon) {

        SimpleDateFormat sdf= new SimpleDateFormat("MMM dd, yyyy");
        Date dateCreated= pricePerGallon.getDateCreated();
        String convertedDateTime= sdf.format(dateCreated);

        return convertedDateTime;
    }

    /** Saving All Details of an Object to send to Other Activity */
    public PricePerGallon getItemInsideArraylist(List<PricePerGallon> itemList, int position) {
        PricePerGallon pricePerGallonIntent= new PricePerGallon();

        pricePerGallonIntent.setPricePerGallon_coconutSize(itemList.get(position).getPricePerGallon_coconutSize());
        pricePerGallonIntent.setPricePerGallon_laborPrice(itemList.get(position).getPricePerGallon_laborPrice());
        pricePerGallonIntent.setPricePerGallon_numberOfCoconut(itemList.get(position).getPricePerGallon_numberOfCoconut());
        pricePerGallonIntent.setPricePerGallon_numberOfLiters(itemList.get(position).getPricePerGallon_numberOfLiters());
        pricePerGallonIntent.setPricePerGallon_numberOfTree(itemList.get(position).getPricePerGallon_numberOfTree());
        pricePerGallonIntent.setPricePerGallon_pricePerLiter(itemList.get(position).getPricePerGallon_pricePerLiter());
        pricePerGallonIntent.setPricePerGallon_productId(itemList.get(position).getPricePerGallon_productId());
        pricePerGallonIntent.setPricerPerGallon_coconutVariety(itemList.get(position).getPricerPerGallon_coconutVariety());
        pricePerGallonIntent.setPricerPerGallon_initialIncome(itemList.get(position).getPricerPerGallon_initialIncome());
        pricePerGallonIntent.setDateCreated(itemList.get(position).getDateCreated());
        pricePerGallonIntent.setId(itemList.get(position).getId());



        return pricePerGallonIntent;

    }

}