package coconut.app.coconapp.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import coconut.app.coconapp.Fragments.Priceperkilo;
import coconut.app.coconapp.Fragments.Priceperpieces;
import coconut.app.coconapp.Fragments.Pricepersako;



public class PagerAdapter extends FragmentStatePagerAdapter {

    int numOfTabs;
    public PagerAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs= numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Priceperkilo ppk= new Priceperkilo();
                if (ppk != null) {
                    ppk.updateFragmentRecyclerView();
                }
                return ppk;

            case 1:
                Pricepersako pps= new Pricepersako();
                return pps;
            case 2:
                Priceperpieces ppp= new Priceperpieces();
                if (ppp != null) {
                    ppp.updateFragmentRecyclerView();
                }
                return ppp;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

   @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
