package coconut.app.coconapp.Adapters;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import org.w3c.dom.Text;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import coconut.app.coconapp.Activities.EditPricePerFruitActivity;
import coconut.app.coconapp.Classes.PricePerFruit;
import coconut.app.coconapp.Classes.PricePerKilo;
import coconut.app.coconapp.Database.DatabaseHelper;
import coconut.app.coconapp.R;

/**
 * Created by EasyBreezy on 2/12/2017.
 */

public class PricePerFruitRecyclerViewAdapter extends RecyclerView.Adapter<PricePerFruitRecyclerViewAdapter.MyViewholder> {

    PricePerFruit pricePerFruitToBeDeleted;

    List<PricePerFruit> pricePerFruitLists;
    String[] longPressChoice= {"Edit","Delete"};
    DatabaseHelper helper=null;
    PricePerFruit pricePerFruit,pricePerFruitIntentInstance;
    Dao<PricePerFruit, Integer> pricePerFruitIntegerDao;

    class MyViewholder extends RecyclerView.ViewHolder {

        TextView numberOfTrees, numberOfFruits, coconutVariety, coconutSizes,fruitPrice, labor, dateCreated, initialIncome;
        public MyViewholder(final View itemView) {
            super(itemView);

            numberOfTrees= (TextView)itemView.findViewById(R.id.numberoftrees_textview);
            numberOfFruits= (TextView)itemView.findViewById(R.id.numberofcoconut_textview);
            coconutVariety= (TextView)itemView.findViewById(R.id.varietyofcoconut_textview);
            coconutSizes= (TextView)itemView.findViewById(R.id.sizeofcoconut_textview);
            fruitPrice=(TextView)itemView.findViewById(R.id.priceperfruit_temp_textview);
            labor= (TextView)itemView.findViewById(R.id.priceperfruit_labor_textview);
            dateCreated= (TextView)itemView.findViewById(R.id.priceperfruit_datecreated_textview);
            initialIncome= (TextView)itemView.findViewById(R.id.priceperfruit_initialincome_textview);

            /** Long Click Listeners */
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View view) {

                    /** Getting ID of the selected item long pressed */
                     final int idPosition= getLayoutPosition();
                    pricePerFruit= new PricePerFruit(idPosition);

                    /** Alert Builder */
                    AlertDialog.Builder builder= new AlertDialog.Builder(itemView.getContext());
                    builder.setTitle("Options");
                    builder.setItems(longPressChoice, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (longPressChoice[i].contentEquals("Edit")) {
                                pricePerFruitIntentInstance= new PricePerFruit();
                                pricePerFruitIntentInstance=getItemsInsideArraylist(pricePerFruitLists,idPosition);

                                Intent intent=new Intent(view.getContext(), EditPricePerFruitActivity.class);
                                intent.putExtra("priceperfruitbundle",pricePerFruitIntentInstance);
                                view.getContext().startActivity(intent);
                                Log.i("Option Selected","Edit");
                            }
                            if (longPressChoice[i].contentEquals("Delete")) {

                                pricePerFruitDialogDelete(view,getAdapterPosition());
                                Log.i("Option Selected","Delete");
                            }
                        }
                    });

                    AlertDialog dialog= builder.create();
                    dialog.show();
                    dialog.setCanceledOnTouchOutside(false);

                    return false;
                }
            });
        }
    }

    @Override
    public MyViewholder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Inflare the View
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.priceperfruit_customlayout,parent,false);

        return new MyViewholder(itemView);
    }




    // Bind the Data to the UI Widgets
    @Override
    public void onBindViewHolder(MyViewholder holder, int position) {
        pricePerFruit= pricePerFruitLists.get(position);
        holder.numberOfTrees.setText(String.valueOf(pricePerFruit.getPricePerFruit_numberOfTrees()));
        holder.numberOfFruits.setText(String.valueOf(pricePerFruit.getPricePerFruit_numberOfFruit()));
        holder.coconutVariety.setText(String.valueOf(pricePerFruit.getPricerPerFruit_coconutVariety()));
        holder.coconutSizes.setText(String.valueOf(pricePerFruit.getPricePerFruit_coconutSize()));
        holder.fruitPrice.setText(String.valueOf(pricePerFruit.getPricePerFruit_pricePerFruit()));
        holder.labor.setText(String.valueOf(pricePerFruit.getLabor()));
        holder.dateCreated.setText(String.valueOf(convertSqliteDateTime(pricePerFruit)));
        holder.initialIncome.setText(String.valueOf(pricePerFruit.getPricePerFruit_initialIncome()));
    }

    @Override
    public int getItemCount() {
        return pricePerFruitLists.size();
    }

    /** Convert SQLITE DateCreated to readable format */
    public String convertSqliteDateTime(PricePerFruit pricePerFruit) {

        SimpleDateFormat sdf= new SimpleDateFormat("MMM dd, yyyy");
        Date dateCreated= pricePerFruit.getDateCreated();
        String convertedDateTime= sdf.format(dateCreated);

        return convertedDateTime;
    }

    /** Saving all content inside the Object Instance to be used in Edit Activity*/
    public PricePerFruit getItemsInsideArraylist(List<PricePerFruit> pricePerFruitLists,int position) {
        PricePerFruit pricePerFruitobj=new PricePerFruit();

        pricePerFruitobj.setId(pricePerFruitLists.get(position).getId());
        pricePerFruitobj.setProductsType(pricePerFruitLists.get(position).getProductsType());
        pricePerFruitobj.setPricePerFruit_numberOfTrees(pricePerFruitLists.get(position).getPricePerFruit_numberOfTrees());
        pricePerFruitobj.setPricePerFruit_numberOfFruit(pricePerFruitLists.get(position).getPricePerFruit_numberOfFruit());
        pricePerFruitobj.setPricerPerFruit_coconutVariety(pricePerFruitLists.get(position).getPricerPerFruit_coconutVariety());
        pricePerFruitobj.setPricePerFruit_coconutSize(pricePerFruitLists.get(position).getPricePerFruit_coconutSize());
        pricePerFruitobj.setPricePerFruit_pricePerFruit(pricePerFruitLists.get(position).getPricePerFruit_pricePerFruit());
        pricePerFruitobj.setLabor(pricePerFruitLists.get(position).getLabor());
        pricePerFruitobj.setPricePerFruit_initialIncome(pricePerFruitLists.get(position).getPricePerFruit_initialIncome());
        pricePerFruitobj.setDateCreated(pricePerFruitLists.get(position).getDateCreated());

        return pricePerFruitobj;
    }

    /** Dialog For Delete */
    private void pricePerFruitDialogDelete(final View v, final  int position) {

        pricePerFruitToBeDeleted= new PricePerFruit();
        pricePerFruitToBeDeleted= getItemsInsideArraylist(pricePerFruitLists, position);

        AlertDialog.Builder builder= new AlertDialog.Builder(v.getContext());
        builder.setTitle("Sure to delete?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                helper= OpenHelperManager.getHelper(v.getContext(),DatabaseHelper.class);

                try {
                    pricePerFruitIntegerDao= helper.getPricePerFruitDao();
                    int status =pricePerFruitIntegerDao.delete(pricePerFruitToBeDeleted);
                    pricePerFruitLists.remove(position);
                    notifyItemRemoved(position);
                    notifyDataSetChanged();
                    pricePerFruitIntegerDao.refresh(pricePerFruitToBeDeleted);

                    Log.i("Item Deletion", String.valueOf(status));
                } catch (SQLException e) {
                    e.printStackTrace();
                }


            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog= builder.create();
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
    }

    public PricePerFruitRecyclerViewAdapter (List<PricePerFruit> pricePerFruitLists) {
        this.pricePerFruitLists= pricePerFruitLists;
    }

}
