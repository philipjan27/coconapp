package coconut.app.coconapp.Adapters;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import coconut.app.coconapp.Activities.EditPricePerKiloValues;
import coconut.app.coconapp.Classes.PricePerKilo;
import coconut.app.coconapp.Database.DatabaseHelper;
import coconut.app.coconapp.R;

/**
        * Created by EasyBreezy on 2/3/2017.
                */

        public class PricePerKiloRecyclerViewAdapter  extends RecyclerView.Adapter<PricePerKiloRecyclerViewAdapter.MyViewHolder>{

           PricePerKilo pricePerKiloToBeDeleted;
            List<PricePerKilo> pricePerKiloIntegerList;
            String[] choices= {"Edit", "Delete"};

            DatabaseHelper helper=null;
            Dao<PricePerKilo,Integer> pricePerKiloIntegerDao;
            PricePerKilo pricePerKilo, intentObject;

            class MyViewHolder extends RecyclerView.ViewHolder {

        TextView numberOfTrees, numberOfCoconut, varietyOfCoconut, coconutSize, numberOfSacks, initialKilo, pricePerkiloTv, laborPrice, dateCreated, initialIncome;
        public MyViewHolder(View itemView) {
            super(itemView);

            numberOfTrees= (TextView)itemView.findViewById(R.id.numberoftrees_textview);
            numberOfCoconut= (TextView)itemView.findViewById(R.id.numberofcoconut_textview);
            varietyOfCoconut= (TextView)itemView.findViewById(R.id.varietyofcoconut_textview);
            coconutSize= (TextView)itemView.findViewById(R.id.sizeofcoconut_textview);
            numberOfSacks= (TextView)itemView.findViewById(R.id.numberofsacks_textview);
            initialKilo= (TextView)itemView.findViewById(R.id.initialkilo_textview);
            pricePerkiloTv= (TextView)itemView.findViewById(R.id.priceperkilo_temp_textview);
            laborPrice= (TextView)itemView.findViewById(R.id.laborPrice_textview);
            dateCreated= (TextView)itemView.findViewById(R.id.dateCreated_textview);
            initialIncome= (TextView)itemView.findViewById(R.id.priceperkilo_initialincome_textview);


            /** Item LongClick Events */
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View view) {

                    /** Getting ID of selected item in the arraylist */
                    final int idPosition= getLayoutPosition();
                    pricePerKilo= new PricePerKilo(idPosition);

                    /** Create AlertBuilder for Choosing DELETE/EDIT */
                    AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(view.getContext());
                    alertDialogBuilder.setTitle("Options");
                    alertDialogBuilder.setItems(choices, new DialogInterface.OnClickListener() {


                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (choices[i].contentEquals("Edit")) {

                                /** Send Data to EditPricePerKiloValues activity */
                                intentObject= new PricePerKilo();
                                intentObject= getItemsInsideArrayList(pricePerKiloIntegerList,idPosition);

                                Intent intent= new Intent(view.getContext(), EditPricePerKiloValues.class);
                                intent.putExtra("ppkobject",intentObject);
                                view.getContext().startActivity(intent);

                                Log.i("Options Selected:","Edit");
                            }
                            if (choices[i].contentEquals("Delete")) {
                                deletePricePerKiloDialog(view,getAdapterPosition());
                                Log.i("Options Selected","Delete");
                            }
                        }
                    });

                    AlertDialog dialog= alertDialogBuilder.create();
                    dialog.show();
                    dialog.setCanceledOnTouchOutside(false);

                    return false;
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.priceperkilo_customlayout,parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
             pricePerKilo= pricePerKiloIntegerList.get(position);

            holder.numberOfTrees.setText(String.valueOf(pricePerKilo.getPricePerKilo_numberofTrees()));
            holder.numberOfCoconut.setText(String.valueOf(pricePerKilo.getPricePerKilo_numberofCoconutfruit()));
            holder.varietyOfCoconut.setText(String.valueOf(pricePerKilo.getPricePerKilo_coconutVariety()));
            holder.coconutSize.setText(String.valueOf(pricePerKilo.getPricePerKilo_size()));
            holder.numberOfSacks.setText(String.valueOf(pricePerKilo.getPricePerKilo_numberOfSacks()));
            holder.initialKilo.setText(String.valueOf(pricePerKilo.getPricePerKilo_initialKilo()));
            holder.pricePerkiloTv.setText(String.valueOf(pricePerKilo.getPricePerKilo_pricePerKilo()));
            holder.laborPrice.setText(String.valueOf(pricePerKilo.getPricePerKilo_laborPrice()));
            holder.dateCreated.setText(String.valueOf(convertSqliteDateTime(pricePerKilo)));
            holder.initialIncome.setText(String.valueOf(pricePerKilo.getPricePerKilo_initialIncome()));

    }

    @Override
    public int getItemCount() {
        return pricePerKiloIntegerList.size();
    }

    public PricePerKiloRecyclerViewAdapter (List<PricePerKilo> pricePerKiloIntegerList) {
        this.pricePerKiloIntegerList= pricePerKiloIntegerList;
    }


    /** Convert SQLITE DateCreated to readable format */
    public String convertSqliteDateTime(PricePerKilo pricePerKilo) {
        SimpleDateFormat sdf= new SimpleDateFormat("MMM dd, yyyy");
        Date dateCreated= pricePerKilo.getDateCreated();
        String convertedDateTime= sdf.format(dateCreated);

        return convertedDateTime;
    }

    /** Saving All Details of an Object to send to Other Activity */
    public PricePerKilo getItemsInsideArrayList(List<PricePerKilo> pricePerKilosList, int position) {
        PricePerKilo pricePerKilo= new PricePerKilo();

        pricePerKilo.setId(pricePerKilosList.get(position).getId());
        pricePerKilo.setPricePerKilo_productsType(pricePerKilosList.get(position).getPricePerKilo_productsType());
        pricePerKilo.setPricePerKilo_numberofTrees(pricePerKilosList.get(position).getPricePerKilo_numberofTrees());
        pricePerKilo.setPricePerKilo_numberofCoconutfruit(pricePerKilosList.get(position).getPricePerKilo_numberofCoconutfruit());
        pricePerKilo.setPricePerKilo_coconutVariety(pricePerKilosList.get(position).getPricePerKilo_coconutVariety());
        pricePerKilo.setPricePerKilo_size(pricePerKilosList.get(position).getPricePerKilo_size());
        pricePerKilo.setPricePerKilo_numberOfSacks(pricePerKilosList.get(position).getPricePerKilo_numberOfSacks());
        pricePerKilo.setPricePerKilo_initialKilo(pricePerKilosList.get(position).getPricePerKilo_initialKilo());
        pricePerKilo.setPricePerKilo_pricePerKilo(pricePerKilosList.get(position).getPricePerKilo_pricePerKilo());
        pricePerKilo.setPricePerKilo_laborPrice(pricePerKilosList.get(position).getPricePerKilo_laborPrice());
        pricePerKilo.setPricePerKilo_initialIncome(pricePerKilosList.get(position).getPricePerKilo_initialIncome());
        pricePerKilo.setDateCreated(pricePerKilosList.get(position).getDateCreated());

        return pricePerKilo;
    }

    /** delete Dialog */
    public void deletePricePerKiloDialog(final View v, final int position) {

        pricePerKiloToBeDeleted= new PricePerKilo();
        pricePerKiloToBeDeleted= getItemsInsideArrayList(pricePerKiloIntegerList,position);

        AlertDialog.Builder builder= new AlertDialog.Builder(v.getContext());
        builder.setMessage("Sure to delete?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                /** get DatabaseHelper instance */

                try {
                    /** delete item choosed based on position */
                    helper= OpenHelperManager.getHelper(v.getContext(), DatabaseHelper.class);
                    pricePerKiloIntegerDao= helper.getPricePerKiloIntegerDao();
                  int result=  pricePerKiloIntegerDao.delete(pricePerKiloToBeDeleted);
                    pricePerKiloIntegerList.remove(position);
                    notifyItemRemoved(position);
                    notifyDataSetChanged();
                    pricePerKiloIntegerDao.refresh(pricePerKiloToBeDeleted);

                    Log.i("Item Deletion",String.valueOf(result));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog= builder.create();
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
    }
}
