package coconut.app.coconapp.Adapters;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import coconut.app.coconapp.Activities.EditPricePerSackActivity;
import coconut.app.coconapp.Classes.PricePerSack;
import coconut.app.coconapp.Database.DatabaseHelper;
import coconut.app.coconapp.R;

/**
 * Created by EasyBreezy on 2/19/2017.
 */

public class PricePerSackRecyclerViewAdapter extends RecyclerView.Adapter<PricePerSackRecyclerViewAdapter.MyViewHolder> {

    List<PricePerSack> pricePerSacksList;
    PricePerSack pricePerSack,intentObject;
    DatabaseHelper dbHelper=null;
    Dao<PricePerSack,Integer> pricePerSackIntegerDao;
    String[] choices= {"Edit", "Delete"};

    PricePerSack pricePerSackToBeDeleted;

public PricePerSackRecyclerViewAdapter(List<PricePerSack> pricePerSacks) {
    this.pricePerSacksList=pricePerSacks;
}

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView numberOfTrees, numberOfFruits,coconutVariety,cocoSize,cocoProductType,
                numberOfSacks,pricePerSackTv, dateCreated, initialIncome;

        public MyViewHolder(final View itemView) {
            super(itemView);

            numberOfTrees= (TextView)itemView.findViewById(R.id.numberoftrees_textview);
            numberOfFruits= (TextView)itemView.findViewById(R.id.numberofcoconut_textview);
            coconutVariety= (TextView)itemView.findViewById(R.id.varietyofcoconut_textview);
            cocoSize= (TextView)itemView.findViewById(R.id.sizeofcoconut_textview);
            cocoProductType= (TextView)itemView.findViewById(R.id.producttype_textview);
            numberOfSacks= (TextView)itemView.findViewById(R.id.numberofsacks_textview);
            pricePerSackTv= (TextView)itemView.findViewById(R.id.pricepersack_temp_textview);
            dateCreated= (TextView)itemView.findViewById(R.id.pricepersack_dateCreated);
            initialIncome= (TextView)itemView.findViewById(R.id.pricepersack_initialincome_textview);

            /** Item LongClick Events */
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View view) {

                    /** Getting ID of selected item in the arraylist */
                    final int idPosition= getLayoutPosition();
                    pricePerSack= new PricePerSack(idPosition);

                    /** Create AlertBuilder for Choosing DELETE/EDIT */
                    AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(view.getContext());
                    alertDialogBuilder.setTitle("Options");
                    alertDialogBuilder.setItems(choices, new DialogInterface.OnClickListener() {


                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (choices[i].contentEquals("Edit")) {

                                /** Send Data to EditPricePerKiloValues activity */
                                intentObject= new PricePerSack();
                                intentObject= getItemsInsideArraylist(pricePerSacksList, idPosition);

                                Intent intent= new Intent(view.getContext(), EditPricePerSackActivity.class);
                                intent.putExtra("pricepersackbundle",intentObject);
                                view.getContext().startActivity(intent);

                                Log.i("Options Selected:","Edit");
                            }
                            if (choices[i].contentEquals("Delete")) {

                                deletePricePerSackDialog(itemView, getAdapterPosition());
                                Log.i("Options Selected","Delete");
                            }
                        }
                    });

                    AlertDialog dialog= alertDialogBuilder.create();
                    dialog.show();
                    dialog.setCanceledOnTouchOutside(false);

                    return false;
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.pricepersako_customlayout,parent,false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // Getting each item and positions to display in textview
        pricePerSack= pricePerSacksList.get(position);

        holder.numberOfTrees.setText(String.valueOf(pricePerSack.getPricePerSack_numberOfTrees()));
        holder.numberOfFruits.setText(String.valueOf(pricePerSack.getPricePerSack_numberOfCoconutl()));
        holder.coconutVariety.setText(pricePerSack.getPricePerSack_coconutVariety());
        holder.cocoSize.setText(pricePerSack.getPricePerSack_coconutSize());
        holder.cocoProductType.setText(pricePerSack.getPricePerSack_productType());
        holder.numberOfSacks.setText(String.valueOf(pricePerSack.getPricePerSack_numberOfSacks()));
        holder.pricePerSackTv.setText(String.valueOf(pricePerSack.getPricePerSack_pricePerSack()));
        holder.dateCreated.setText(convertSqliteDateTime(pricePerSack));
        holder.initialIncome.setText(String.valueOf(pricePerSack.getPricePerSack_initialIncome()));
    }

    @Override
    public int getItemCount() {
        return pricePerSacksList.size();
    }

    /** Convert SQLITE DateCreated to readable format */
    public String convertSqliteDateTime(PricePerSack pricePerSack) {
       SimpleDateFormat sdf= new SimpleDateFormat("MMM dd, yyyy");
        Date dateCreated= pricePerSack.getDateCreated();
        String convertedDateTime= sdf.format(dateCreated);

        return convertedDateTime;
    }

    /** Getting Items of 1 object to be edited in Edit Activity */
    public PricePerSack getItemsInsideArraylist(List<PricePerSack> pricePerSacksList, int position) {
        PricePerSack pricePerSack= new PricePerSack();

        pricePerSack.setId(pricePerSacksList.get(position).getId());
        pricePerSack.setDateCreated(pricePerSacksList.get(position).getDateCreated());
        pricePerSack.setPricePerSack_coconutSize(pricePerSacksList.get(position).getPricePerSack_coconutSize());
        pricePerSack.setPricePerSack_numberOfTrees(pricePerSacksList.get(position).getPricePerSack_numberOfTrees());
        pricePerSack.setPricePerSack_coconutVariety(pricePerSacksList.get(position).getPricePerSack_coconutVariety());
        pricePerSack.setPricePerSack_initialIncome(pricePerSacksList.get(position).getPricePerSack_initialIncome());
        pricePerSack.setProductsType(pricePerSacksList.get(position).getProductsType());
        pricePerSack.setPricePerSack_numberOfCoconutl(pricePerSacksList.get(position).getPricePerSack_numberOfCoconutl());
        pricePerSack.setPricePerSack_numberOfSacks(pricePerSacksList.get(position).getPricePerSack_numberOfSacks());
        pricePerSack.setPricePerSack_pricePerSack(pricePerSacksList.get(position).getPricePerSack_pricePerSack());
        pricePerSack.setPricePerSack_productType(pricePerSacksList.get(position).getPricePerSack_productType());

        return pricePerSack;
    }

    /** Deleting and Item */
    public void deletePricePerSackDialog(final View v,final int position) {

        pricePerSackToBeDeleted= new PricePerSack();
        pricePerSackToBeDeleted= getItemsInsideArraylist(pricePerSacksList, position);

        AlertDialog.Builder builder= new AlertDialog.Builder(v.getContext());
        builder.setMessage("Sure to delete?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                /** get DatabaseHelper instance */
                dbHelper= OpenHelperManager.getHelper(v.getContext(), DatabaseHelper.class);
                try {

                    /** delete item choosed based on position */
                    pricePerSackIntegerDao= dbHelper.getPricePerSackIntegerDao();
                  int status= pricePerSackIntegerDao.delete(pricePerSackToBeDeleted);
                    notifyItemRemoved(position);
                    pricePerSacksList.remove(position);
                    pricePerSackIntegerDao.refresh(pricePerSackToBeDeleted);
                    notifyDataSetChanged();

                    Log.i("Item Deletion",String.valueOf(status));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog= builder.create();
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
    }
}
