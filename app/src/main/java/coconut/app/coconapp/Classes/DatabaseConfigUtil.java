package coconut.app.coconapp.Classes;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.File;
import java.io.IOError;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by EasyBreezy on 1/7/2017.
 */

public class DatabaseConfigUtil extends OrmLiteConfigUtil{
    private static final Class<?>[] classes= new Class[] {
            PricePerFruit.class,PricePerGallon.class, PricePerKilo.class, PricePerSack.class, ProductsType.class
    };

    public static void main(String[] args) throws SQLException, IOException {
        writeConfigFile(new File("C:/Users/EasyBreezy/Documents/AndroidStudioProjects/CoconApp/app/src/main/res/raw/ormlite_config.txt"),classes);
    }
}
