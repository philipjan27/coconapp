package coconut.app.coconapp.Classes;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.sql.SQLRecoverableException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import coconut.app.coconapp.Database.DatabaseHelper;

/**
 * Created by EasyBreezy on 3/6/2017.
 */

public class MonthlyDataRetrievalUtil {

    public static String JAN="01";
    public static String FEB="02";
    public static String MAR="03";
    public static String APR="04";
    public static String MAY="05";
    public static String JUN="06";
    public static String JUL="07";
    public static String AUG="08";
    public static String SEP="09";
    public static String OCT="10";
    public static String NOV="11";
    public static String DEC="12";
    private int YEAR= getCurrentyear();



    PricePerGallon pricePerGallonObj;
    PricePerFruit pricePerFruitObj;
    PricePerKilo pricePerKiloObj;
    PricePerSack pricePerSackObj;

    Dao<PricePerGallon, Integer> pricePerGallonsDao;
    Dao<PricePerKilo, Integer> pricePerKiloIntegerDao;
    Dao<PricePerFruit, Integer> pricePerFruitIntegerDao;
    Dao<PricePerSack, Integer> pricePerSackIntegerDao;

    DatabaseHelper  dbHelper=null;


    Context ctx;

    public MonthlyDataRetrievalUtil() {

    }

    public MonthlyDataRetrievalUtil(Context _ctx) {
        this.ctx= _ctx;
    }


    public DatabaseHelper getHelper() {
        if (dbHelper== null) {
            dbHelper= OpenHelperManager.getHelper(ctx,DatabaseHelper.class);
        }

        return dbHelper;
    }


    /**
     *  Getting Quarter Income of Pricepergallon
     *  Parameter is Months
     *  */
    public String[] getOverallIncomeInSingleQuarterPricePerGallon(String monthBelow, String monthAbove) throws SQLException {

        //Getting dao instance
        try {
            pricePerGallonsDao= getHelper().getPricePerGallonIntegerDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        GenericRawResults<String[]> totalIncomePerQuarter= pricePerGallonsDao.queryRaw(
                "SELECT SUM(initial_income) from tbl_pricepergallon_data WHERE strftime('%Y-%m',date_created) >= "
                +"'"+getCurrentyear()+"-"+monthBelow+"'"+" AND "+"strftime('%Y-%m',date_created) <= "+"'"+getCurrentyear()+"-"+monthAbove+"'"+"");

        List<String[]> pricePerGallonQuarterlyIncomeList= totalIncomePerQuarter.getResults();
        String[] resultArray= pricePerGallonQuarterlyIncomeList.get(0);

        return resultArray;

    }


/**
 * Getting Quarter Income of Priceperkilo
 * Parameter is Months
 */
    public String[] getOverallIncomeInSingleQuarterPricePerKilo(String monthBelow, String monthAbove) throws SQLException {

        try {
            pricePerKiloIntegerDao= getHelper().getPricePerKiloIntegerDao();
        }catch (SQLException e) {
            e.printStackTrace();
        }

        GenericRawResults<String[]> totalIncomePerQuarter= pricePerKiloIntegerDao.queryRaw(
                "SELECT SUM(initial_income) from tbl_productsperkilo_data WHERE strftime('%Y-%m',date_created) >= "
                        +"'"+getCurrentyear()+"-"+monthBelow+"'"+" AND "+"strftime('%Y-%m',date_created) <= "+"'"+getCurrentyear()+"-"+monthAbove+"'"+"");

        List<String[]> pricePerKiloQuarterlyIncomeList= totalIncomePerQuarter.getResults();
        String[] resultArray= pricePerKiloQuarterlyIncomeList.get(0);

        return resultArray;
    }

/**
 * Getting Quarter Income of Priceperfruit
 * Paremeter is Months
 */
    public String[] getOverAllIncomeInSingleQuarterPricePerFruit(String monthBelow, String monthAbove) throws SQLException {

        try {
            pricePerFruitIntegerDao= getHelper().getPricePerFruitDao();
        }catch (SQLException e) {
            e.printStackTrace();
        }

        GenericRawResults<String[]> totalIncomePerQuarter= pricePerFruitIntegerDao.queryRaw(
                "SELECT SUM(initial_income) from tbl_productsperfruit_data WHERE strftime('%Y-%m',date_created) >= "
                +"'"+getCurrentyear()+"-"+monthBelow+"'"+" AND "+"strftime('%Y-%m',date_created) <= "+"'"+getCurrentyear()+"-"+monthAbove+"'"+"");

        List<String[]> pricePerFruitQuarterlyIncomeList= totalIncomePerQuarter.getResults();
        String[] result= pricePerFruitQuarterlyIncomeList.get(0);

        return result;

    }

/**
 * Getting Quarter Income of Pricepersack
 * Parameter is Months
  */
    public String[] getOverAllIncomeInSingleQuarterPricePerSack(String monthBelow, String monthAbove) throws SQLException{

        try {
            pricePerSackIntegerDao= getHelper().getPricePerSackIntegerDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        GenericRawResults<String[]> totalIncomePerQuarter= pricePerSackIntegerDao.queryRaw(
                "SELECT SUM(initial_income) from tbl_productspersack_data WHERE strftime('%Y-%m',date_created) >= "
                        +"'"+getCurrentyear()+"-"+monthBelow+"'"+" AND "+"strftime('%Y-%m',date_created) <= "+"'"+getCurrentyear()+"-"+monthAbove+"'"+"");

        List<String[]> pricePerSackIncomeList= totalIncomePerQuarter.getResults();
        String[] result= pricePerSackIncomeList.get(0);

        return result;
    }

    /**
     * Getting Current Year in yyyy format
     * */
    public int getCurrentyear() {

        int year= Calendar.getInstance().get(Calendar.YEAR);

        Log.i("Current Year",String.valueOf(year));
        return year;
    }

    /**
     * Getting Current year in MM format
     * @return
     */
    public static String getCurrentMonth() {

        String monthInString="";
        int currentMonth= Calendar.getInstance().get(Calendar.MONTH)+1;
        if (currentMonth < 10) {
            monthInString="0"+String.valueOf(currentMonth);
        }else {
            monthInString= String.valueOf(currentMonth);
        }

        return monthInString;
    }



}
