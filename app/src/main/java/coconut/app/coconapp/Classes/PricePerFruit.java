package coconut.app.coconapp.Classes;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by EasyBreezy on 1/7/2017.
 */

@DatabaseTable(tableName = "tbl_productsperfruit_data")
public class PricePerFruit implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = "product_id",foreign = true,foreignAutoRefresh = true,canBeNull = false)
    private ProductsType productsType;

    @DatabaseField(columnName = "numberof_trees",canBeNull = false)
    private int pricePerFruit_numberOfTrees;

    @DatabaseField(columnName = "numberof_coconuts",canBeNull = false)
    private int pricePerFruit_numberOfFruit;

    @DatabaseField(columnName = "varietyof_coconut",canBeNull = false)
    private String pricerPerFruit_coconutVariety;

    @DatabaseField(columnName ="sizeof_coconut",canBeNull = false)
    private String pricePerFruit_coconutSize;

    @DatabaseField(columnName = "priceper_coconut",canBeNull = false, dataType = DataType.DOUBLE)
    private double pricePerFruit_pricePerFruit;

    @DatabaseField(columnName = "labor", canBeNull = false, dataType = DataType.DOUBLE)
    private double labor;

    @DatabaseField(columnName = "initial_income", dataType = DataType.DOUBLE)
    private double pricePerFruit_initialIncome;

    @DatabaseField(columnName = "date_created", dataType = DataType.DATE_STRING,  format = "yyyy-MM-dd HH:mm:ss")
    public Date dateCreated;

    public PricePerFruit() {

    }

    public PricePerFruit(int _id) {
        this.id= _id;
    }

    public PricePerFruit(int id, ProductsType productsType, int pricePerFruit_numberOfTrees, int pricePerFruit_numberOfFruit,
                         String pricerPerFruit_coconutVariety, String pricePerFruit_coconutSize, double pricePerFruit_pricePerFruit,
                         double pricePerFruit_labor, double pricePerFruit_initialIncome, Date dateCreated) {
        this.id = id;
        this.productsType = productsType;
        this.pricePerFruit_numberOfTrees = pricePerFruit_numberOfTrees;
        this.pricePerFruit_numberOfFruit = pricePerFruit_numberOfFruit;
        this.pricerPerFruit_coconutVariety = pricerPerFruit_coconutVariety;
        this.pricePerFruit_coconutSize = pricePerFruit_coconutSize;
        this.pricePerFruit_pricePerFruit = pricePerFruit_pricePerFruit;
        this.labor=pricePerFruit_labor;
        this.pricePerFruit_initialIncome = pricePerFruit_initialIncome;
        this.dateCreated=dateCreated;
    }

    public PricePerFruit(ProductsType productsType, int pricePerFruit_numberOfTrees, int pricePerFruit_numberOfFruit,
                         String pricerPerFruit_coconutVariety, String pricePerFruit_coconutSize,
                         double pricePerFruit_pricePerFruit, double pricePerFruit_labor, double pricePerFruit_initialIncome, Date dateCreated) {
        this.productsType = productsType;
        this.pricePerFruit_numberOfTrees = pricePerFruit_numberOfTrees;
        this.pricePerFruit_numberOfFruit = pricePerFruit_numberOfFruit;
        this.pricerPerFruit_coconutVariety = pricerPerFruit_coconutVariety;
        this.pricePerFruit_coconutSize = pricePerFruit_coconutSize;
        this.pricePerFruit_pricePerFruit = pricePerFruit_pricePerFruit;
        this.labor=pricePerFruit_labor;
        this.pricePerFruit_initialIncome = pricePerFruit_initialIncome;
        this.dateCreated=dateCreated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductsType getProductsType() {
        return productsType;
    }

    public void setProductsType(ProductsType productsType) {
        this.productsType = productsType;
    }

    public int getPricePerFruit_numberOfTrees() {
        return pricePerFruit_numberOfTrees;
    }

    public void setPricePerFruit_numberOfTrees(int pricePerFruit_numberOfTrees) {
        this.pricePerFruit_numberOfTrees = pricePerFruit_numberOfTrees;
    }

    public int getPricePerFruit_numberOfFruit() {
        return pricePerFruit_numberOfFruit;
    }

    public void setPricePerFruit_numberOfFruit(int pricePerFruit_numberOfFruit) {
        this.pricePerFruit_numberOfFruit = pricePerFruit_numberOfFruit;
    }

    public String getPricerPerFruit_coconutVariety() {
        return pricerPerFruit_coconutVariety;
    }

    public void setPricerPerFruit_coconutVariety(String pricerPerFruit_coconutVariety) {
        this.pricerPerFruit_coconutVariety = pricerPerFruit_coconutVariety;
    }

    public String getPricePerFruit_coconutSize() {
        return pricePerFruit_coconutSize;
    }

    public void setPricePerFruit_coconutSize(String pricePerFruit_coconutSize) {
        this.pricePerFruit_coconutSize = pricePerFruit_coconutSize;
    }

    public double getPricePerFruit_pricePerFruit() {
        return pricePerFruit_pricePerFruit;
    }

    public void setPricePerFruit_pricePerFruit(double pricePerFruit_pricePerFruit) {
        this.pricePerFruit_pricePerFruit = pricePerFruit_pricePerFruit;
    }

    public double getLabor() {
        return labor;
    }

    public void setLabor(double labor) {
        this.labor = labor;
    }

    public double getPricePerFruit_initialIncome() {
        return pricePerFruit_initialIncome;
    }

    public void setPricePerFruit_initialIncome(double pricePerFruit_initialIncome) {
        this.pricePerFruit_initialIncome = pricePerFruit_initialIncome;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }


}
