package coconut.app.coconapp.Classes;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by EasyBreezy on 1/7/2017.
 */

@DatabaseTable(tableName = "tbl_pricepergallon_data")
public class PricePerGallon implements Serializable{

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField(columnName = "product_id", foreign = true, foreignAutoRefresh = true, canBeNull = false)
    public ProductsType pricePerGallon_productId;

    @DatabaseField(columnName = "numberof_tree")
    public int pricePerGallon_numberOfTree;

    @DatabaseField(columnName = "numberof_coconut")
    public int pricePerGallon_numberOfCoconut;

    @DatabaseField(columnName = "coconut_variety")
    public String pricerPerGallon_coconutVariety;

    @DatabaseField(columnName = "coconut_size")
    public String pricePerGallon_coconutSize;

    @DatabaseField(columnName = "numberof_liters", dataType = DataType.DOUBLE)
    public double pricePerGallon_numberOfLiters;

    @DatabaseField(columnName = "priceper_liter", dataType = DataType.DOUBLE)
    public double pricePerGallon_pricePerLiter;

    @DatabaseField(columnName = "labor", dataType = DataType.DOUBLE)
    public double pricePerGallon_laborPrice;

    @DatabaseField(columnName = "initial_income", dataType = DataType.DOUBLE)
    public double pricerPerGallon_initialIncome;

    @DatabaseField(columnName = "date_created", dataType = DataType.DATE_STRING,  format = "yyyy-MM-dd HH:mm:ss")
    public Date dateCreated;

    public PricePerGallon() {

    }

    public PricePerGallon(int id, ProductsType pricePerGallon_productId, int pricePerGallon_numberOfTree, int pricePerGallon_numberOfCoconut,
                          String pricerPerGallon_coconutVariety, String pricePerGallon_coconutSize, double pricePerGallon_numberOfLiters, double pricePerGallon_pricePerLiter, double pricePerGallon_laborPrice, double pricerPerGallon_initialIncome,Date dateCreated) {
        this.id = id;
        this.pricePerGallon_productId = pricePerGallon_productId;
        this.pricePerGallon_numberOfTree = pricePerGallon_numberOfTree;
        this.pricePerGallon_numberOfCoconut = pricePerGallon_numberOfCoconut;
        this.pricerPerGallon_coconutVariety = pricerPerGallon_coconutVariety;
        this.pricePerGallon_coconutSize = pricePerGallon_coconutSize;
        this.pricePerGallon_numberOfLiters = pricePerGallon_numberOfLiters;
        this.pricePerGallon_pricePerLiter = pricePerGallon_pricePerLiter;
        this.pricePerGallon_laborPrice = pricePerGallon_laborPrice;
        this.pricerPerGallon_initialIncome = pricerPerGallon_initialIncome;
        this.dateCreated=dateCreated;
    }

    public PricePerGallon(ProductsType pricePerGallon_productId, int pricePerGallon_numberOfTree, int pricePerGallon_numberOfCoconut,
                          String pricerPerGallon_coconutVariety, String pricePerGallon_coconutSize, double pricePerGallon_numberOfLiters, double pricePerGallon_pricePerLiter, double pricePerGallon_laborPrice, double pricerPerGallon_initialIncome,Date dateCreated) {

        this.pricePerGallon_productId = pricePerGallon_productId;
        this.pricePerGallon_numberOfTree = pricePerGallon_numberOfTree;
        this.pricePerGallon_numberOfCoconut = pricePerGallon_numberOfCoconut;
        this.pricerPerGallon_coconutVariety = pricerPerGallon_coconutVariety;
        this.pricePerGallon_coconutSize = pricePerGallon_coconutSize;
        this.pricePerGallon_numberOfLiters = pricePerGallon_numberOfLiters;
        this.pricePerGallon_pricePerLiter = pricePerGallon_pricePerLiter;
        this.pricePerGallon_laborPrice = pricePerGallon_laborPrice;
        this.pricerPerGallon_initialIncome = pricerPerGallon_initialIncome;
        this.dateCreated=dateCreated;
    }

    public PricePerGallon(int pricePerGallon_numberOfTree, int pricePerGallon_numberOfCoconut,
                          String pricerPerGallon_coconutVariety, String pricePerGallon_coconutSize, double pricePerGallon_numberOfLiters, double pricePerGallon_pricePerLiter, double pricePerGallon_laborPrice, double pricerPerGallon_initialIncome,Date dateCreated) {

        this.pricePerGallon_numberOfTree = pricePerGallon_numberOfTree;
        this.pricePerGallon_numberOfCoconut = pricePerGallon_numberOfCoconut;
        this.pricerPerGallon_coconutVariety = pricerPerGallon_coconutVariety;
        this.pricePerGallon_coconutSize = pricePerGallon_coconutSize;
        this.pricePerGallon_numberOfLiters = pricePerGallon_numberOfLiters;
        this.pricePerGallon_pricePerLiter = pricePerGallon_pricePerLiter;
        this.pricePerGallon_laborPrice = pricePerGallon_laborPrice;
        this.pricerPerGallon_initialIncome = pricerPerGallon_initialIncome;
        this.dateCreated=dateCreated;
    }

    public PricePerGallon(int id) {
        this.id=id;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductsType getPricePerGallon_productId() {
        return pricePerGallon_productId;
    }

    public void setPricePerGallon_productId(ProductsType pricePerGallon_productId) {
        this.pricePerGallon_productId = pricePerGallon_productId;
    }

    public int getPricePerGallon_numberOfTree() {
        return pricePerGallon_numberOfTree;
    }

    public void setPricePerGallon_numberOfTree(int pricePerGallon_numberOfTree) {
        this.pricePerGallon_numberOfTree = pricePerGallon_numberOfTree;
    }

    public int getPricePerGallon_numberOfCoconut() {
        return pricePerGallon_numberOfCoconut;
    }

    public void setPricePerGallon_numberOfCoconut(int pricePerGallon_numberOfCoconut) {
        this.pricePerGallon_numberOfCoconut = pricePerGallon_numberOfCoconut;
    }

    public String getPricerPerGallon_coconutVariety() {
        return pricerPerGallon_coconutVariety;
    }

    public void setPricerPerGallon_coconutVariety(String pricerPerGallon_coconutVariety) {
        this.pricerPerGallon_coconutVariety = pricerPerGallon_coconutVariety;
    }

    public String getPricePerGallon_coconutSize() {
        return pricePerGallon_coconutSize;
    }

    public void setPricePerGallon_coconutSize(String pricePerGallon_coconutSize) {
        this.pricePerGallon_coconutSize = pricePerGallon_coconutSize;
    }

    public double getPricePerGallon_numberOfLiters() {
        return pricePerGallon_numberOfLiters;
    }

    public void setPricePerGallon_numberOfLiters(double pricePerGallon_numberOfLiters) {
        this.pricePerGallon_numberOfLiters = pricePerGallon_numberOfLiters;
    }

    public double getPricePerGallon_pricePerLiter() {
        return pricePerGallon_pricePerLiter;
    }

    public void setPricePerGallon_pricePerLiter(double pricePerGallon_pricePerLiter) {
        this.pricePerGallon_pricePerLiter = pricePerGallon_pricePerLiter;
    }

    public double getPricePerGallon_laborPrice() {
        return pricePerGallon_laborPrice;
    }

    public void setPricePerGallon_laborPrice(double pricePerGallon_laborPrice) {
        this.pricePerGallon_laborPrice = pricePerGallon_laborPrice;
    }

    public double getPricerPerGallon_initialIncome() {
        return pricerPerGallon_initialIncome;
    }

    public void setPricerPerGallon_initialIncome(double pricerPerGallon_initialIncome) {
        this.pricerPerGallon_initialIncome = pricerPerGallon_initialIncome;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
