package coconut.app.coconapp.Classes;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.beans.PropertyChangeEvent;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by EasyBreezy on 1/6/2017.
 */

@DatabaseTable(tableName = "tbl_productsperkilo_data")
public class PricePerKilo implements Serializable {


    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false,foreign = true, foreignAutoRefresh = true, columnName = "product_id")
    private ProductsType  pricePerKilo_productsType;

    @DatabaseField(columnName = "numberof_trees", canBeNull = false)
    private int pricePerKilo_numberofTrees;

    @DatabaseField(columnName = "numberof_coconut",canBeNull = false)
    private int pricePerKilo_numberofCoconutfruit;

    @DatabaseField(columnName = "varietyof_coconut",canBeNull = false)
    private String pricePerKilo_coconutVariety;

    @DatabaseField(columnName = "sizeof_coconut",canBeNull = false)
    private String pricePerKilo_size;

    @DatabaseField(columnName = "numberof_sacks",canBeNull = false)
    private int pricePerKilo_numberOfSacks;

    @DatabaseField(columnName = "initial_kilo",canBeNull = false, dataType = DataType.DOUBLE)
    private double pricePerKilo_initialKilo;

    @DatabaseField(columnName = "priceper_kilo",canBeNull = false, dataType = DataType.DOUBLE)
    private  double pricePerKilo_pricePerKilo;

    @DatabaseField(columnName = "labor_price",canBeNull = false, dataType = DataType.DOUBLE)
    private double pricePerKilo_laborPrice;

    @DatabaseField(columnName = "initial_income",canBeNull = false, dataType = DataType.DOUBLE)
    private double pricePerKilo_initialIncome;

    @DatabaseField(columnName = "date_created", dataType = DataType.DATE_STRING,  format = "yyyy-MM-dd HH:mm:ss")
    public Date dateCreated;

    public PricePerKilo () {

    }

    public PricePerKilo(int _id, ProductsType productsType, int numberOfTrees, int numberofCoconut,
                             String coconutVariety, String perKiloSize, int numberOfSacks, double initialKilo, double pricePerKilo, double laborPrice, double initialIncome,Date dateCreated) {

        this.id=_id;
        this.pricePerKilo_productsType= productsType;
        this.pricePerKilo_numberofTrees= numberOfTrees;
        this.pricePerKilo_numberofCoconutfruit= numberofCoconut;
        this.pricePerKilo_coconutVariety= coconutVariety;
        this.pricePerKilo_size= perKiloSize;
        this.pricePerKilo_numberOfSacks= numberOfSacks;
        this.pricePerKilo_initialKilo=initialKilo;
        this.pricePerKilo_pricePerKilo=pricePerKilo;
        this. pricePerKilo_laborPrice= laborPrice;
        this.pricePerKilo_initialIncome= initialIncome;
        this.dateCreated=dateCreated;


    }

    public PricePerKilo(ProductsType productsType, int numberOfTrees, int numberofCoconut,
                        String coconutVariety, String perKiloSize, int numberOfSacks, double initialKilo, double pricePerKilo, double laborPrice, double initialIncome,Date dateCreated) {

        this.pricePerKilo_productsType= productsType;
        this.pricePerKilo_numberofTrees= numberOfTrees;
        this.pricePerKilo_numberofCoconutfruit= numberofCoconut;
        this.pricePerKilo_coconutVariety= coconutVariety;
        this.pricePerKilo_size= perKiloSize;
        this.pricePerKilo_numberOfSacks= numberOfSacks;
        this.pricePerKilo_initialKilo=initialKilo;
        this.pricePerKilo_pricePerKilo=pricePerKilo;
        this.pricePerKilo_numberOfSacks= numberOfSacks;
        this. pricePerKilo_laborPrice= laborPrice;
        this.pricePerKilo_initialIncome=initialIncome;
        this.dateCreated=dateCreated;

    }

    public PricePerKilo(int id) {
        this.id= id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductsType getPricePerKilo_productsType() {
        return pricePerKilo_productsType;
    }

    public void setPricePerKilo_productsType(ProductsType pricePerKilo_productsType) {
        this.pricePerKilo_productsType = pricePerKilo_productsType;
    }

    public int getPricePerKilo_numberofTrees() {
        return pricePerKilo_numberofTrees;
    }

    public void setPricePerKilo_numberofTrees(int pricePerKilo_numberofTrees) {
        this.pricePerKilo_numberofTrees = pricePerKilo_numberofTrees;
    }

    public int getPricePerKilo_numberofCoconutfruit() {
        return pricePerKilo_numberofCoconutfruit;
    }

    public void setPricePerKilo_numberofCoconutfruit(int pricePerKilo_numberofCoconutfruit) {
        this.pricePerKilo_numberofCoconutfruit = pricePerKilo_numberofCoconutfruit;
    }

    public String getPricePerKilo_coconutVariety() {
        return pricePerKilo_coconutVariety;
    }

    public void setPricePerKilo_coconutVariety(String pricePerKilo_coconutVariety) {
        this.pricePerKilo_coconutVariety = pricePerKilo_coconutVariety;
    }

    public String getPricePerKilo_size() {
        return pricePerKilo_size;
    }

    public void setPricePerKilo_size(String pricePerKilo_size) {
        this.pricePerKilo_size = pricePerKilo_size;
    }

    public int getPricePerKilo_numberOfSacks() {
        return pricePerKilo_numberOfSacks;
    }

    public void setPricePerKilo_numberOfSacks(int pricePerKilo_numberOfSacks) {
        this.pricePerKilo_numberOfSacks = pricePerKilo_numberOfSacks;
    }

    public double getPricePerKilo_initialKilo() {
        return pricePerKilo_initialKilo;
    }

    public void setPricePerKilo_initialKilo(double pricePerKilo_initialKilo) {
        this.pricePerKilo_initialKilo = pricePerKilo_initialKilo;
    }

    public double getPricePerKilo_pricePerKilo() {
        return pricePerKilo_pricePerKilo;
    }

    public void setPricePerKilo_pricePerKilo(double pricePerKilo_pricePerKilo) {
        this.pricePerKilo_pricePerKilo = pricePerKilo_pricePerKilo;
    }

    public double getPricePerKilo_laborPrice() {
        return pricePerKilo_laborPrice;
    }

    public void setPricePerKilo_laborPrice(double pricePerKilo_laborPrice) {
        this.pricePerKilo_laborPrice = pricePerKilo_laborPrice;
    }

    public double getPricePerKilo_initialIncome() {
        return pricePerKilo_initialIncome;
    }

    public void setPricePerKilo_initialIncome(double pricePerKilo_initialIncome) {
        this.pricePerKilo_initialIncome = pricePerKilo_initialIncome;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
