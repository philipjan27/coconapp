package coconut.app.coconapp.Classes;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by EasyBreezy on 1/6/2017.
 */

@DatabaseTable(tableName = "tbl_productspersack_data")
public class PricePerSack implements Serializable{

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = "product_id",foreign = true,foreignAutoRefresh = true,canBeNull = false)
    private ProductsType productsType;

    @DatabaseField(columnName = "numberof_trees",canBeNull = false)
    private int pricePerSack_numberOfTrees;

    @DatabaseField(columnName = "numberof_coconut",canBeNull = false)
    private int pricePerSack_numberOfCoconutl;

    @DatabaseField(columnName = "varietyof_coconut",canBeNull = false)
    private String pricePerSack_coconutVariety;

    @DatabaseField(columnName = "sizeof_coconut",canBeNull = false)
    private String pricePerSack_coconutSize;

    @DatabaseField(columnName = "coconutproduct_type",canBeNull = false)
    private String pricePerSack_productType;

    @DatabaseField(columnName = "numberof_sacks")
    private int pricePerSack_numberOfSacks;

    @DatabaseField(columnName = "priceper_sack", dataType = DataType.DOUBLE)
    private double pricePerSack_pricePerSack;

    @DatabaseField(columnName = "initial_income", dataType = DataType.DOUBLE)
    private double pricePerSack_initialIncome;

    @DatabaseField(columnName = "date_created", dataType = DataType.DATE_STRING,  format = "yyyy-MM-dd HH:mm:ss")
    public Date dateCreated;

    public PricePerSack() {

    }

    public PricePerSack(int _id) {
        this.id=_id;
    }
    public PricePerSack(int id, ProductsType productsType, int pricePerSack_numberOfTrees, int pricePerSack_numberOfCoconutl,
                        String pricePerSack_coconutVariety, String pricePerSack_coconutSize, String pricePerSack_productType,
                        int pricePerSack_numberOfSacks, double pricePerSack_pricePerSack, double pricePerSack_initialIncome,Date dateCreated) {
        this.id = id;
        this.productsType = productsType;
        this.pricePerSack_numberOfTrees = pricePerSack_numberOfTrees;
        this.pricePerSack_numberOfCoconutl = pricePerSack_numberOfCoconutl;
        this.pricePerSack_coconutVariety = pricePerSack_coconutVariety;
        this.pricePerSack_coconutSize = pricePerSack_coconutSize;
        this.pricePerSack_productType = pricePerSack_productType;
        this.pricePerSack_numberOfSacks = pricePerSack_numberOfSacks;
        this.pricePerSack_pricePerSack = pricePerSack_pricePerSack;
        this.pricePerSack_initialIncome = pricePerSack_initialIncome;
        this.dateCreated=dateCreated;
    }

    public PricePerSack( ProductsType productsType, int pricePerSack_numberOfTrees, int pricePerSack_numberOfCoconutl,
                        String pricePerSack_coconutVariety, String pricePerSack_coconutSize, String pricePerSack_productType,
                        int pricePerSack_numberOfSacks, double pricePerSack_pricePerSack, double pricePerSack_initialIncome,Date dateCreated) {
        this.productsType = productsType;
        this.pricePerSack_numberOfTrees = pricePerSack_numberOfTrees;
        this.pricePerSack_numberOfCoconutl = pricePerSack_numberOfCoconutl;
        this.pricePerSack_coconutVariety = pricePerSack_coconutVariety;
        this.pricePerSack_coconutSize = pricePerSack_coconutSize;
        this.pricePerSack_productType = pricePerSack_productType;
        this.pricePerSack_numberOfSacks = pricePerSack_numberOfSacks;
        this.pricePerSack_pricePerSack = pricePerSack_pricePerSack;
        this.pricePerSack_initialIncome = pricePerSack_initialIncome;
        this.dateCreated=dateCreated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductsType getProductsType() {
        return productsType;
    }

    public void setProductsType(ProductsType productsType) {
        this.productsType = productsType;
    }

    public int getPricePerSack_numberOfTrees() {
        return pricePerSack_numberOfTrees;
    }

    public void setPricePerSack_numberOfTrees(int pricePerSack_numberOfTrees) {
        this.pricePerSack_numberOfTrees = pricePerSack_numberOfTrees;
    }

    public int getPricePerSack_numberOfCoconutl() {
        return pricePerSack_numberOfCoconutl;
    }

    public void setPricePerSack_numberOfCoconutl(int pricePerSack_numberOfCoconutl) {
        this.pricePerSack_numberOfCoconutl = pricePerSack_numberOfCoconutl;
    }

    public String getPricePerSack_coconutVariety() {
        return pricePerSack_coconutVariety;
    }

    public void setPricePerSack_coconutVariety(String pricePerSack_coconutVariety) {
        this.pricePerSack_coconutVariety = pricePerSack_coconutVariety;
    }

    public String getPricePerSack_coconutSize() {
        return pricePerSack_coconutSize;
    }

    public void setPricePerSack_coconutSize(String pricePerSack_coconutSize) {
        this.pricePerSack_coconutSize = pricePerSack_coconutSize;
    }

    public String getPricePerSack_productType() {
        return pricePerSack_productType;
    }

    public void setPricePerSack_productType(String pricePerSack_productType) {
        this.pricePerSack_productType = pricePerSack_productType;
    }

    public int getPricePerSack_numberOfSacks() {
        return pricePerSack_numberOfSacks;
    }

    public void setPricePerSack_numberOfSacks(int pricePerSack_numberOfSacks) {
        this.pricePerSack_numberOfSacks = pricePerSack_numberOfSacks;
    }

    public double getPricePerSack_pricePerSack() {
        return pricePerSack_pricePerSack;
    }

    public void setPricePerSack_pricePerSack(double pricePerSack_pricePerSack) {
        this.pricePerSack_pricePerSack = pricePerSack_pricePerSack;
    }

    public double getPricePerSack_initialIncome() {
        return pricePerSack_initialIncome;
    }

    public void setPricePerSack_initialIncome(double pricePerSack_initialIncome) {
        this.pricePerSack_initialIncome = pricePerSack_initialIncome;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
