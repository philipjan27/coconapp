package coconut.app.coconapp.Classes;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by EasyBreezy on 1/5/2017.
 */

@DatabaseTable(tableName = "tbl_products_type")
public class ProductsType implements Serializable {


    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false, columnName = "product_type")
    private String productType;

    public ProductsType() {

    }

    public ProductsType(int _id, String pType) {
        this.id=_id;
        this.productType=pType;
    }

    public ProductsType(String pType) {
        this.productType=pType;
    }

    public ProductsType(int _id) {
        this.id=_id;
    }
}
