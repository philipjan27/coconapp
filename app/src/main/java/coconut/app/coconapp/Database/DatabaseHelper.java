package coconut.app.coconapp.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;

import coconut.app.coconapp.Classes.PricePerFruit;
import coconut.app.coconapp.Classes.PricePerGallon;
import coconut.app.coconapp.Classes.PricePerKilo;
import coconut.app.coconapp.Classes.PricePerSack;
import coconut.app.coconapp.Classes.ProductsType;
import coconut.app.coconapp.Fragments.Priceperpieces;
import coconut.app.coconapp.R;

/**
 * Created by EasyBreezy on 1/7/2017.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME= "coconapp.db";
    private  static final int DATABASE_VERSION=1;

    // Dao
    private Dao<PricePerFruit, Integer> pricePerFruitIntegerDao;
    private Dao<PricePerGallon,Integer> pricePerGallonIntegerDao;
    private Dao<PricePerKilo,Integer> pricePerKiloIntegerDao;
    private  Dao<PricePerSack,Integer> pricePerSackIntegerDao;
    private Dao<ProductsType,Integer> productsTypes;
    private Dao<Priceperpieces,Integer> priceperpieces;

    // Constructor
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource,PricePerFruit.class);
            TableUtils.createTable(connectionSource,PricePerGallon.class);
            TableUtils.createTable(connectionSource,PricePerKilo.class);
            TableUtils.createTable(connectionSource,PricePerSack.class);
            TableUtils.createTable(connectionSource,ProductsType.class);

        }catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(),e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }

    public Dao<ProductsType, Integer> getProductsTypeDao() throws SQLException {
        if (productsTypes == null) {
            productsTypes= getDao(ProductsType.class);
        }

        return productsTypes;
    }

    public Dao<PricePerFruit,Integer> getPricePerFruitDao()throws SQLException {
        if (pricePerFruitIntegerDao == null) {
            pricePerFruitIntegerDao = getDao(PricePerFruit.class);
        }

        return pricePerFruitIntegerDao;
    }


    public Dao<PricePerGallon, Integer>getPricePerGallonIntegerDao()throws SQLException {
        if (pricePerGallonIntegerDao == null) {
            pricePerGallonIntegerDao= getDao(PricePerGallon.class);
        }
        return pricePerGallonIntegerDao;
    }

    public Dao<PricePerKilo, Integer>getPricePerKiloIntegerDao()throws SQLException {
        if (pricePerKiloIntegerDao == null) {
            pricePerKiloIntegerDao= getDao(PricePerKilo.class);
        }

        return pricePerKiloIntegerDao;
    }

    public Dao<PricePerSack,Integer>getPricePerSackIntegerDao()throws SQLException {
        if (pricePerSackIntegerDao == null) {
            pricePerSackIntegerDao= getDao(PricePerSack.class);
        }

        return pricePerSackIntegerDao;
    }
    public Dao<Priceperpieces,Integer>getpriceperpiecesIntegerDao()throws SQLException {
        if (priceperpieces == null) {
            priceperpieces= getDao(Priceperpieces.class);
        }

        return priceperpieces;
    }


}
