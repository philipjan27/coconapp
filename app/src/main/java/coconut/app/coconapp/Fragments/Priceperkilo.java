package coconut.app.coconapp.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coconut.app.coconapp.Adapters.PricePerKiloRecyclerViewAdapter;
import coconut.app.coconapp.Classes.PricePerKilo;
import coconut.app.coconapp.Database.DatabaseHelper;
import coconut.app.coconapp.R;

public class Priceperkilo extends Fragment{

    final static int PRICE_PER_KILO = 4;
    Dao<PricePerKilo,Integer> pricePerKiloIntegerDao;
    List<PricePerKilo> pricePerKiloList=null;
    List<PricePerKilo> reversedPricePerKilo= null;
    DatabaseHelper helper=null;
    RecyclerView recyclerView;
    PricePerKiloRecyclerViewAdapter pricePerKiloRecyclerViewAdapter= null;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            savedInstanceState.clear();
            savedInstanceState=null;
        }


        if (pricePerKiloIntegerDao == null) {
            try {
                pricePerKiloIntegerDao=getHelper().getPricePerKiloIntegerDao();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

           pricePerKiloList= getAllDatas();

            /** Reverse the data position
             * Added values should be in top instead of bottom*/
            Collections.reverse(pricePerKiloList);
            reversedPricePerKilo= pricePerKiloList;

        pricePerKiloRecyclerViewAdapter= new PricePerKiloRecyclerViewAdapter(reversedPricePerKilo);

        Log.i("onCreate","on create");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_priceperkilo, container, false);

        recyclerView = (RecyclerView)v.findViewById(R.id.priceperkilo_recyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(pricePerKiloRecyclerViewAdapter);

        pricePerKiloRecyclerViewAdapter.notifyDataSetChanged();

        Log.i("onCreateView","on create view");
        return v;
    }

    /** Update Fragments RecyclerView when resumed */
    @Override
    public void onResume() {
        pricePerKiloRecyclerViewAdapter.notifyDataSetChanged();

        Log.i("onResume","on resume");
        super.onResume();
    }

    @Override
    public void onStart() {

        Log.i("onStart","on start");
        super.onStart();
    }

    @Override
    public void onPause() {

        Log.i("onPause","on pause");
        super.onPause();
    }

    @Override
    public void onStop() {

        Log.i("onStop","on stop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (helper != null) {
            OpenHelperManager.releaseHelper();
            helper=null;
        }
        Log.i("onDestroy","on destroy");
    }

    /** Getting DatabaseHelper class instance */
    private DatabaseHelper getHelper() {
        if (helper == null) {
            helper= OpenHelperManager.getHelper(getActivity(),DatabaseHelper.class);
        }
        return helper;
    }

    /** Getting All Datas in database */
    public List<PricePerKilo> getAllDatas() {

        pricePerKiloList= new ArrayList<>();
        try {
            pricePerKiloList= pricePerKiloIntegerDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pricePerKiloList;
    }

    public void updateFragmentRecyclerView() {
        if (pricePerKiloRecyclerViewAdapter != null) {
            pricePerKiloRecyclerViewAdapter.notifyDataSetChanged();
        }

    }


}
