package coconut.app.coconapp.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coconut.app.coconapp.Adapters.PricePerFruitRecyclerViewAdapter;
import coconut.app.coconapp.Classes.PricePerFruit;
import coconut.app.coconapp.Database.DatabaseHelper;
import coconut.app.coconapp.R;

public class Priceperpieces extends Fragment {

    Dao<PricePerFruit, Integer> pricePerFruitIntegerDao=null;
    List<PricePerFruit>pricePerFruitList;
    List<PricePerFruit> reversedPricePerFruit;
    DatabaseHelper dbHelper= null;
    RecyclerView recyclerView;
    PricePerFruitRecyclerViewAdapter pricePerFruitRecyclerViewAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            savedInstanceState.clear();
            savedInstanceState=null;
        }

        if (pricePerFruitIntegerDao == null) {
            try {
                pricePerFruitIntegerDao=getHelper().getPricePerFruitDao();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        pricePerFruitList= getAllDatas();

        Collections.reverse(pricePerFruitList);

        reversedPricePerFruit= pricePerFruitList;

        pricePerFruitRecyclerViewAdapter = new PricePerFruitRecyclerViewAdapter(reversedPricePerFruit);

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        pricePerFruitRecyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        if (dbHelper != null) {
            OpenHelperManager.releaseHelper();
            dbHelper= null;
        }

        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_priceperpieces, container, false);


        recyclerView= (RecyclerView)v.findViewById(R.id.pricerfruit_recyclerview);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(pricePerFruitRecyclerViewAdapter);

        pricePerFruitRecyclerViewAdapter.notifyDataSetChanged();
        return v;
    }

    public DatabaseHelper getHelper() {
        if (dbHelper == null) {
            dbHelper= OpenHelperManager.getHelper(getActivity(),DatabaseHelper.class);
        }

        return dbHelper;
    }

    public List<PricePerFruit> getAllDatas() {
        pricePerFruitList= new ArrayList<>();

        try {
            pricePerFruitList= pricePerFruitIntegerDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pricePerFruitList;
    }

    public void updateFragmentRecyclerView() {
        if (pricePerFruitRecyclerViewAdapter != null) {
            pricePerFruitRecyclerViewAdapter.notifyDataSetChanged();
        }
    }

}
