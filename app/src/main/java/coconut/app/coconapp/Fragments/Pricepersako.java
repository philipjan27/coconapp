package coconut.app.coconapp.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coconut.app.coconapp.Adapters.PricePerSackRecyclerViewAdapter;
import coconut.app.coconapp.Classes.PricePerSack;
import coconut.app.coconapp.Database.DatabaseHelper;
import coconut.app.coconapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Pricepersako extends Fragment {

    Dao<PricePerSack, Integer> pricePerSackIntegerDao;
    List<PricePerSack> pricePerSacksList;
    List<PricePerSack> reversedPricePerSacksList;
    DatabaseHelper dbHelper=null;
    PricePerSack pricePerSack;
    PricePerSackRecyclerViewAdapter pricePerSackRecyclerViewAdapter;
    RecyclerView recyclerView;

    public Pricepersako() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (pricePerSackIntegerDao == null) {
            try {
                pricePerSackIntegerDao= getHelper().getPricePerSackIntegerDao();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        pricePerSacksList= getAllDatas();
        Collections.reverse(pricePerSacksList);
        reversedPricePerSacksList= new ArrayList<>();
        reversedPricePerSacksList= pricePerSacksList;

        pricePerSackRecyclerViewAdapter= new PricePerSackRecyclerViewAdapter(reversedPricePerSacksList);
        pricePerSackRecyclerViewAdapter.notifyDataSetChanged();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_pricepersako, container, false);

        recyclerView= (RecyclerView)v.findViewById(R.id.pricepersack_recyclerview);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(pricePerSackRecyclerViewAdapter);
        pricePerSackRecyclerViewAdapter.notifyDataSetChanged();

        return v;
    }

    public DatabaseHelper getHelper() {
        if (dbHelper == null) {
            dbHelper= OpenHelperManager.getHelper(getActivity(),DatabaseHelper.class);
        }

        return dbHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (dbHelper != null) {
            OpenHelperManager.releaseHelper();

            dbHelper= null;
        }
    }

    public List<PricePerSack> getAllDatas() {
        pricePerSacksList=new ArrayList<>();

        try {
            pricePerSacksList= pricePerSackIntegerDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pricePerSacksList;
    }
}
